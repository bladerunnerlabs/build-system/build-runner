#!/bin/bash

if [[ "${TERM}" =~ "xterm" ]]; then
    RED='\033[0;31m'
    GREEN='\033[0;32m'
    BLUE='\033[0;34m'
    YELLOW='\033[1;33m'
    NC='\033[0m' # No Color
else
    RED=
    GREEN=
    BLUE=
    YELLOW=
    NC=
fi

echo_red() {
    echo -e ${RED}$@${NC}
}

echo_green() {
    echo -e ${GREEN}$@${NC}
}

echo_blue() {
    echo -e ${BLUE}$@${NC}
}

echo_yellow() {
    echo -e ${YELLOW}$@${NC}
}

print_color() {
    local color="$1"; shift
    case ${color} in
        (red|RED)
            echo_red "$@" ;;
        (green|GREEN)
            echo_green "$@" ;;
        (blue|BLUE)
            echo_blue "$@" ;;
        (yellow|YELLOW)
            echo_yellow "$@" ;;
        (*) 
            echo_red "BUG: unsupported color: ${color}" 
            echo -e "$@"
            ;;
    esac
}

print_n() {
    if [ -z "${QUIET}" ]; then echo -e "$@"; fi
}

print_nc() {
    if [ -z "${QUIET}" ]; then print_color "$@"; fi
}

print_v() {
    if [ -n "${VERBOSE}" ]; then echo -e "$@"; fi
}

print_vc() {
    if [ -n "${VERBOSE}" ]; then print_color -e "$@"; fi
}

function prefix_line()
{
    sed -n "s/.*/[${dir}] &/p"
}

function fatal_error()
{
    ret_code=${1:-0}; shift
    echo_red "$@" | prefix_line
    exit ${ret_code}
}

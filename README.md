# `build-runner` - a tool for multi-repository projects

[[_TOC_]]

**build-runner** is a tool for managing projects comprised of multiple Git repositories. It covers both Git and build functionality, striving to present a consistent unified view of the project. Project configuration is described by a **manifest** YAML file.

The main command of **build-runner** is `project`.

During the build phase `CMake` is supported as the primary build tool. **build-runner** may work with vanilla `CMake` projects but is intended to work in conjunction with **cmake-runner**, although these frameworks can function independently. Support for additional build tools (e.g. `make`, `meson` etc) may be added in the future.

## Command line interface of `project`

```
Usage: ./project [command] [sub-cmd] <module-list> <options> -- <extra-opts>

Global options (autocomp presents relevant ones for each command):
	--manifest -M : use custom manifest file, default: manifest.yaml
	--serial -s : process all modules sequentially
	--makefile -m : show temp Makefile created to handle a command
	--root -R : set project root directory, default: parent of build-runner
	--force -f : perform some commands uncoditionally and/or ignore errors
	--verbose -v : produce verbose output
	--quiet -q : produce minimal or no output

Commands (autocomp presents relevant sub-commands):
	<module-list> may be supplied to most commands, default: all modules

git - perform git ops on modules, usually according to the manifest
	fetch - git fetch modules, by the manifest and upstream if relevant
	sync - update by the manifest, give up if inconsistent (--force applicable)
	pull - git pull modules, if inconsistent with manifest, use upstream
		'sync' and 'pull' fetch and do a dry-run, checking current git state,
		then proceed only if all modules can be updated;
		infrastructure modules are auto-upgraded (as in: git sync -M infra.yaml)
		--nofetch -F : do not perform git fetch (if done by default)
		--noinfra -I : do not synchronize infra modules
		--check -c : only the dry-run check is performed, no git update
		--force -f : git update is performed unconditionally;
		  note that it may leave some modules unupdated
	push - push modules to their git repositories
	reset - git reset modules
	status - show status for all modules
	diff - show diff for all modules

	-- <extra-opts> are passed to git, e.g. --depth, --name-only, --dry-run

module - process modules separately according to the manifest dependencies
	cmake - CMake generate and configure step for requested modules
	build - build modules
	install - install requested modules
		--target -t : custom target for all affected modules
		--cmake -c : define CMake variables using -D, e.g. -c X=1
		--gen -g {make|ninja} : use this generator
		--nodeps -N : build requested modules only, ignore dependencies
		--noprebuild -P : do not execute pre-build step for all modules
		-- <extra-opts> are passed to 'cmake --build', e.g. -j
	clean - clean requested modules
	purge - remove directories of requested modules
		--install -i : remove installed files of requested modules
		--build -b : remove binary dir of requested modules
		--tar -t : remove all tars of requested modules
	pack - tar pack requested modules (use tarpack yaml config)
	unpack - tar unpack requested module
		 --file -f : tgz path to unpack
		e.g unpack <module_name> --file <tar_file>

tree - generate and build a single-tree CMake project
	gen - create the root CMakeLists.txt for the single-tree project
	cmake - configure single-tree CMake project
	build - single-tree CMake project build
	install - single-tree CMake project install

list - show modules list and info (-v shows full info)
	all - all modules
	cmake - only modules with platform: cmake or cmake-runner

autocomp - generate auto-complete script
```

## Project workflow

### Minimal environment
Project repositories are supposed to be derived from [`project-template`](https://gitlab.com/bladerunnerlabs/build-system/project-template).

After being "bootstapped" and pushed to its own git repository, the project environment has the "infrastructure" sub-directories, at least:

- `build-runner`
- `cmake-runner`
- `pack-runner`

and at least the following files, all required for the regular workflow:

- `project` - symbolic link to the `project` script in `build-runner` sub-directory
- `init-project.sh` - script responsible for initial setup of the infrastructure
- `manifest.yaml` - configuration file describing the project constituent repositories, there inter-dependencies and build process parameters
- `infra.yaml` - configuration file describing the infrastructure constituent repositories

All routine multi-repo git, build and miscellaneous tasks may be performed using `project` script.

The difference between `git sync` and `git pull` is that `sync` tries to update local git repos in accordance with the manifest, while `pull` tries to update the local branches (possibly different from the manifest) synchronizing them with their upstream remotes.

If `sync` detects this kind of discrepancy, it leaves the local branch without updating. If `--force` is passed, `sync` will unconditionally check out the up-to-date version according to the manifest. Note that `--force` should synchronize additional aspects like the Git remote's URL, upstream branches etc. Without `--force` flag a corresponding warning and/or instructions for manual fix are printed.

The insfrastructure may be updated using `init-project.sh` at any time. This may be also achieved by running: `project git sync -M infra.yaml`.

Running `git pull`, `git sync` and `git status` fetches remote git repositories. This may be disabled by using `--nofetch`(`-F`) option.

Running `git pull` and `git sync` automatically updates infrastructure modules, if possible. This may be disabled by using `--noinfra`(`-I`) option.

### `project` script modes of operation
The `project` script operates on multiples modules, each one derived from a separate git repository.

It executes most of the commands in parallel, e.g. updating the project modules from their git repositories or building the modules. To achieve the parallelism a temporary `Makefile` is generated and called with appropriate parameters. The actions performed by the generated `Makefile` are transparent to the user, although the parallel mode of operation may be inhibited using a special `--serial` parameter, where relevant. For debugging purposes the generated `Makefile` may be shown using `--makefile` (or `-m`) option.

Most commands and operations rely on `manifest.yaml` as the single source of information on the modules and their associated parameters and attributes. Some commands, like `git pull` may rely on the git state of specific local repositories.

### Shell Autocompletion
Output of `project autocomplete` may be sourced by the shell manually or automatically (using any appropriate mechanism) and will provide autocompletion functionality.

## Manifest file format
Configuration file `manifest.yaml` contains the project definitions. 

It is organized into few sections, described below.

### Section: `format`
```yaml
format:
    type: manifest
    version: <version x.y.z>  # current: 1.2.0
```

### Section: `project`
```yaml
project:
    name: <project-name>
    version: <version x.y.z>
    install-prefix: <path>
    env: # dictionary indexed by var-name
        <var-name>:
            type: <string|path>
            value: <value-string>
```

### Section: `project`
```yaml
pack:
    out-dir: <path>
    tags: # array of tag names
        - <tag-name>
```

### Section: `cmake`
```yaml
cmake:
    min-version: <cmake-minimal-version>
    build-type: <Release|Debug>
    generator: <ninja|make>
    cmake-defines: # array of CMake symbol definitions
        - <cmake-var-name>=<value>
    # array of CMake symbol names not to be accounted for during build signature generation
    exclude-from-signature: # since 1.2.0
        - <cmake-var-name>
    # define languages and their attributes, at least a name should be supplied
    # language-related definitions must be provided on command line or in the first lines
    # of the root CMakeLists.txt, before the first project() statement, this is required
    # to allow Cmake identify the right toolchains correctly.
    languages: # since 1.2.0
        - name: <language-name> # CMake notation: CXX, C, ASM etc.
          compiler: <compiler-path>
          launcher: <launcher-path> # used to launch the compiler, like ccache
          standard: <standard-id> # e.g. C++ standards: 14, 17, 20 etc.
```
### Section: `modules`
```yaml
modules: # dictionary indexed by module-name
    <module-name>: # what follows is a <module-definition>
        url: <git-repo-url> # required
        revision: <branch|tag|commit-id> # required
        remote: <local-git-remote-name> # optional, default: origin
        alias: <alias-name> # may be referred by alias in dependencies etc.
        platform: <cmake-runner|cmake|none> # optional, default: cmake-runner
        build-type: <Release|Debug> # optional, overrides cmake level prefix for the module
        cmake-defines: # optional, array of CMake symbol definitions
            - <cmake-var-name>=<value>
        cmake:
            # language settings in a module scope extend or override the language settings defined
            # in the project scope. at least a name should be supplied
            languages: # since 1.2.1
                - name: <language-name> # CMake notation: CXX, C, ASM etc.
                compiler: <compiler-path>
                launcher: <launcher-path> # used to launch the compiler, like ccache
                standard: <standard-id> # e.g. C++ standards: 14, 17, 20 etc.
        pre-build: # optional, array
            - <command> # to run before calling cmake
        build-targets: # optional, array, default: [ all ]
            - <target-name> # passed as '-t <target-name>' to 'cmake --build'
        post-install: # optional, array
            - <command> # to run after install
        depends: # optional, array
            - <module-name> # on which the module depends
        build: <False|True> # since 1.2.0; optional, default: True; no build if False 
        install: <False|True> # optional, default: True; no install if False
        install-prefix: <path> # optional, overrides project level prefix for the module
        root: <root-path> # optional, module root path relative to the project dir if located deeper into module
        env: # dictionary indexed by variable-name, optional
            <varaible-name>: # name of environment variable
                type: <path|string> # required
                value: <string-value> # required for <string>
                scope: <module|project>
        pack:
            yamls: # array of pack-runner config files
                - <pack-yaml-path>
            tags: # array of tag names
                - <tag-name> # adds to project level tags for the module
            out-dir: <path> # optional, overrides project level output-dir for the module
        # list of submodules stored within the module's repo
        submodules: # since 1.2.0; optional, dictionary of submodules defined identically to a module
            <submodule-name>:
                <module-definition>
```

## Project Infrastructure file format
Configuration file `infra.yaml` contains the project infrastructure definitions. 

It is organized into few sections, described below.

### Section: `format`
```yaml
format:
    type: infra
    version: <version x.y.z> # current: 1.0.0
```

### Section: `project`
```yaml
project:
    name: <infra-name>  # build-runner-infra
```

### Section: `modules`
```yaml
modules: # dictionary indexed by module-name
    <module-name>:
        url: <git-repo-url> # required
        revision: <branch|tag|commit-id> # required
        remote: <local-git-remote-name> # optional, default: origin
```

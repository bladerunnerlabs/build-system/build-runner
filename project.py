from posixpath import join
import sys
import glob, os
import subprocess
import shutil
import re
import hashlib
from enum import Enum
try:
    import yaml
    import argparse
    from pykwalify.core import Core as SchemaValidator
    from pykwalify.errors import SchemaError
except ModuleNotFoundError:
    sys.exit(13)

module_info_defaults = {
    "platform": "none",
    "remote": "origin",
    "develop": False,
    "build": True,
    "install": True,
    "build-targets": ["all"]
}


class InstallOp(Enum):
    DO_NOTHING = 0
    DO_UNPACK = 1
    DO_INSTALL = 3


system_defines_list = ["-DCMAKE_INSTALL_PREFIX", "-DCMAKE_BUILD_TYPE", "-DV"]
forbidden_defines_list = ["-DBUILD_ROOT_DIR"]
build_info_exclude_defines_list = [
    "-DCMAKE_INSTALL_PREFIX", "-DBUILD_ROOT_DIR", "-DCMAKE_RUNNER_DIR",
    "-DCMAKE_PREFIX_PATH"
]

git_modes_list = [
    "sync", "reset", "push", "status", "diff", "fetch", "pull", "check"
]

build_info_cache_dict = {}


def print2e(msg):
    print(msg, file=sys.stderr)


def err_print(err_msg):
    print2e(err_msg)
    sys.exit(1)


def usage(retcode):
    print2e("Usage: project.py <path> {git | gen | build | list | module}")
    print2e("  git sync - pull by rebase all modules from their remotes")
    print2e("  git push - push all development modules to their remotes")
    print2e("  git reset - reset all development modules")
    print2e("  build - all modules separately according to dependencies")
    print2e("  gen - generate root cmake file for the project")
    print2e("  list all - list names of all modules")
    print2e("  list git - list names of all modules maintained as git repos")
    print2e("  list cmake - list names of all modules buildable by cmake")
    print2e(
        "  module check (cmake|all) <module1 [module2]...> - module names validation"
    )
    sys.exit(retcode)


def err_usage(err_msg):
    print2e(err_msg)
    print2e("")
    usage(1)


def list_modules_validation(chk_modules_list, manifest_modules_list):
    for module_name in chk_modules_list:
        if not module_name in manifest_modules_list:
            return False, module_name
    return True, ""


def print_full_info_module(module_name, module_info):
    if "depends" in module_info and module_info["depends"]:
        depends = ', '.join(module_info["depends"])
    else:
        depends = "-"

    if "parent_module" not in module_info:
        print(
            "%s\n\turl: \t\t%s\n\tremote: \t%s\n\trevision: \t%s\n\tplatform: \t%s\n\tbuild:\t\t%s\n\tinstall: \t%s\n\tdepends: \t%s"
            % (module_name, module_info["url"], module_info["remote"],
               module_info["revision"], module_info["platform"],
               module_info["build"], module_info["install"], depends))
        if "submodules" in module_info:
            print("\tsubmodules: \t%s" %
                  ", ".join(module_info["submodules"].keys()))
    else:
        print(
            "%s\n\tsubmodule of: \t%s\n\tplatform: \t%s\n\tbuild:\t\t%s\n\tinstall: \t%s\n\tdepends: \t%s"
            % (module_name, module_info["parent_module"],
               module_info["platform"], module_info["build"],
               module_info["install"], depends))


def list_module_names(modules, full_info):
    for name in modules:
        if full_info:
            print_full_info_module(name, modules[name])
        else:
            print(name)


def git_pull_sync_exec(modules_name_list, modules, git_op, script_dir, check,
                       force, git_op_args):
    all_prepared_ok = True
    for name in modules_name_list:
        cmd = script_dir + '/git_cmd.sh ' + git_op + '_prepare --dir ' + name + \
                ' --url ' + modules[name]["url"] + ' --remote ' + modules[name]["remote"] + \
                ' --revision ' + modules[name]["revision"] + ' --root ' + root_dir
        if check:
            cmd += " --check"
        if force:
            cmd += " --force"

        rc, out = subprocess.getstatusoutput(cmd)
        modules[name]["prepare-rc"] = rc
        modules[name]["prepare-out"] = out

        if rc > 1:  # 0: cmd, 1: msg, else error
            all_prepared_ok = False

    exec_succeeded = all_prepared_ok or force
    exit_code = 0 if exec_succeeded else 1

    for name in modules_name_list:
        rc = modules[name]["prepare-rc"]
        out = modules[name]["prepare-out"]
        if rc == 0:
            if exec_succeeded:  # have to execute cmd for this module
                git_cmd = out
                git_rc, git_out = subprocess.getstatusoutput(git_cmd)
                print2e(git_out)
                exit_code = 21 if git_rc == 0 else git_rc
        else:
            print2e(out)

    exit(exit_code)


def git_cmd_handler(modules_name_list, modules, git_op, script_dir, check,
                    force, flow, git_op_args):
    if git_op == "pull" or git_op == "sync":
        git_pull_sync_exec(modules_name_list, modules, git_op, script_dir,
                           check, force, git_op_args)
    else:
        if git_op == "fetch":
            git_op += "_" + flow

        generate_git_makefile(modules_name_list, modules, git_op, script_dir,
                              check, force, git_op_args)


def generate_git_makefile(modules_name_list, modules, git_op, script_dir,
                          check, force, git_op_args):
    print("T=", end='')
    for name in modules_name_list:
        print(name, end=' ')
    print("\nall: $(T)\n.PHONY: all $(T)\n")

    for name in modules_name_list:
        print(
            "%s:\n\t@%s/git_cmd.sh %s --dir %s --url %s --remote %s --revision %s --root %s"
            % (name, script_dir, git_op, name, modules[name]["url"],
               modules[name]["remote"], modules[name]["revision"], root_dir),
            end='')

        if check:
            print(" --check", end='')

        if force:
            print(" --force", end='')

        if git_op_args:
            print(" -- %s" % (git_op_args), end='')

        print("\n")


def print_project_vars(project_env, target):
    for var_name in project_env:
        env_var = project_env[var_name]
        if env_var["type"] == "string":
            if "value" not in env_var:
                err_print("project env var:{} has no 'value'".format(var_name))
            print("%s: export %s=%s" % (target, var_name, env_var["value"]))


def print_module_vars(modules_dict, target, scope):
    for module_name in modules_dict:
        module_info = modules_dict[module_name]
        if "env" not in module_info:
            continue
        for var_name in module_info["env"]:
            env_var = module_info["env"][var_name]

            var_scope = env_var["scope"] if "scope" in env_var else "project"

            if scope != var_scope:
                continue

            if scope == "module" and target != module_name:
                continue

            if env_var["type"] == "path":
                print("%s: export %s=%s/%s" %
                      (target, var_name, root_dir, module_name))
            elif env_var["type"] == "string":
                if "value" not in env_var:
                    err_print("module:{} env var:{} has no 'value'".format(
                        module_name, var_name))
                print("%s: export %s=%s" %
                      (target, var_name, env_var["value"]))


def cmake_define_validation(key, defines_dict):
    if key in forbidden_defines_list:
        err_print("key %s is not allowed to be set by user" % (key))

    if key in defines_dict and key in system_defines_list:
        err_print("key %s should be set in manifest, not on the cmd line" %
                  (key))


def _add_user_define(defines_dict, key, value, force):
    if not key[0:2] == "-D":
        key = "-D" + key
    if not force:
        cmake_define_validation(key, defines_dict)
    defines_dict[key] = value


def add_user_defines(defines_dict, defines_container, force=False):
    if not defines_container:
        return

    if isinstance(defines_container, list):
        for var_def in defines_container:
            key, value = var_def.split('=')
            _add_user_define(defines_dict, key, value, force)
    elif isinstance(defines_container, dict):
        for key, value in defines_container.items():
            _add_user_define(defines_dict, key, value, force)


def cache_module_build_info(module_name, module_build_info):
    build_info_cache_dict[module_name] = module_build_info


def get_cached_module_build_info(module_name):
    if module_name not in build_info_cache_dict:
        err_print("BUG: %s module not found in build info cache" %
                  (module_name))

    return build_info_cache_dict[module_name]


def module_build_info_to_signature_string(build_info):
    build_info_signature = build_info["signature"]

    signature_str = build_info_signature["build-type"]
    signature_str += "-commit-" + build_info_signature["commit-id"]
    if "diff-hash" in build_info_signature:
        signature_str += '-diff-' + build_info_signature["diff-hash"]
    signature_str += "-build-" + build_info_signature["build-id"]

    return signature_str


def tree_build_info_to_signature_string(build_info):
    build_info_signature = build_info["signature"]

    signature_str = build_info_signature["build-type"]
    signature_str += "-build-" + build_info_signature["build-id"]

    return signature_str


def load_build_info_yaml(module_name, build_info_yaml):
    build_info = load_yaml(build_info_yaml)
    if "signature" not in build_info:
        err_print("ERROR: no 'signature' key in: %s" % (build_info_yaml))

    cache_module_build_info(module_name, build_info)

    return build_info


def save_build_info_yaml(module_name, build_info_yaml, build_info):
    save_yaml(build_info_yaml, build_info)
    if "signature" not in build_info:
        err_print("BUG: no 'signature' key for: %s" % (build_info_yaml))

    cache_module_build_info(module_name, build_info)


def find_unique_file(file_pattern):
    file_list = glob.glob(file_pattern)
    num_files = len(file_list)
    if num_files == 1:
        return file_list[0]
    elif num_files == 0:
        return None
    else:
        err_print("ERROR: multiple files for pattern: %s" % (file_pattern))


def remove_files_by_pattern(path_pattern):
    fileList = glob.glob(path_pattern)
    # Iterate over the list of filepaths & remove each file.
    for filePath in fileList:
        try:
            os.remove(filePath)
        except:
            err_print("Error while deleting file : ", filePath)


def create_build_info_cmake_defines_list(defines_dict, exclude_list):
    sorted_symbols = []

    for key in sorted(defines_dict):
        if key not in exclude_list:
            sorted_symbols.append(key + '=' + str(defines_dict[key]))

    return sorted_symbols


def create_module_build_info_dict(module_name, module_info, cmake_generator,
                                  defines_dict, build_targets):
    module_path = root_dir + "/" + module_info["full_name"]

    signature = {}
    signature["commit-id"] = git_head_commit_id_str(module_path)
    signature["build-type"] = module_info["build-type-lower"]

    diff_hash = git_diff_hash_str(module_path)
    if diff_hash:
        signature["diff-hash"] = diff_hash

    build_info = {}
    build_info["signature"] = signature
    build_info["build-targets"] = build_targets

    info_cmake = {}
    info_cmake["generator"] = cmake_generator
    info_cmake["cmake-defines"] = create_build_info_cmake_defines_list(
        defines_dict, build_info_exclude_defines_list)
    build_info["cmake"] = info_cmake

    if "depends" in module_info:
        build_info["depends"] = {}
        for dep_name in module_info["depends"]:
            dep_build_info = get_cached_module_build_info(dep_name)
            dep_build_id = dep_build_info["signature"]["build-id"]
            build_info["depends"][dep_name] = dep_build_id

    # generate build-id (signature hash) from everything except the build-id itself
    build_info["signature"]["build-id"] = build_info_hash_str(build_info)

    return build_info


def generate_module_build_info_yaml(module_name, module_info, cmake_generator,
                                    defines_dict, build_targets,
                                    installed_only, binary_dir_path,
                                    install_prefix):

    if not installed_only:  # all dependencies are built, regular case
        # generate a new module build info dictionary, incl. its build-id (signature hash)
        # and all its dependencies with their respective build-ids
        build_info = create_module_build_info_dict(module_name, module_info,
                                                   cmake_generator,
                                                   defines_dict, build_targets)
        # make sure the binary dir exists and contains no stale signature files
        if os.path.isdir(binary_dir_path):
            remove_files_by_pattern(binary_dir_path + "/init-" +
                                    build_info["signature"]["build-type"] +
                                    "-commit-*.yaml")
        else:
            os.makedirs(binary_dir_path)

        # save the build info dictionary to temp file
        signature_str = module_build_info_to_signature_string(build_info)
        init_build_info_yaml = binary_dir_path + "/init-" + signature_str + ".yaml"
        save_build_info_yaml(module_name, init_build_info_yaml, build_info)

    else:  # installed_only
        # module is assumed to be installed, only installed dir is used
        install_path = install_prefix + "/" + module_name
        file_pattern = "install-" + module_info[
            "build-type-lower"] + "-commit-*.yaml"
        install_build_info_yaml = find_unique_file(install_path + "/" +
                                                   file_pattern)
        if not install_build_info_yaml:
            print2e("module: %s is assumed to be installed" % (module_name))
            err_print("ERROR: no %s files found in %s" %
                      (file_pattern, install_path))

        build_info = load_build_info_yaml(module_name, install_build_info_yaml)
        signature_str = module_build_info_to_signature_string(build_info)

    return signature_str


def prepare_single_tree_lists(single_tree_modules_name_list, modules_dict,
                              force):

    full_build_subtree_list, top_level_modules = create_build_subtree(
        single_tree_modules_name_list, modules_dict)
    single_tree_modules_name_list = order_modules_list_by_subtree(
        single_tree_modules_name_list, full_build_subtree_list)

    independent_modules_name_list = []
    for module_name in full_build_subtree_list:
        if module_name not in single_tree_modules_name_list:
            independent_modules_name_list.append(module_name)

    circular_dep_list = []
    for module_name in independent_modules_name_list:
        module_info = modules_dict[module_name]

        if "depends" not in module_info:
            continue

        for dep_name in module_info["depends"]:
            if dep_name in single_tree_modules_name_list:
                if not force:
                    err_print(
                        "error: circular dependency detected, %s (not in tree) depends on %s (in tree)\n"
                        % (module_name, dep_name))
                else:
                    circular_dep_list.append(module_name)
                    break  # dep_name already listed as creating a circular dep

    for module_name in circular_dep_list:
        single_tree_modules_name_list.append(module_name)
        independent_modules_name_list.remove(module_name)

    return single_tree_modules_name_list, independent_modules_name_list


def create_build_subtree(modules_name_list, modules_dict):
    build_subtree_list = []
    top_level_modules = modules_name_list.copy()

    add_build_subtree(modules_name_list,
                      build_subtree_list,
                      top_level_modules,
                      modules_dict,
                      recursive_level=0)
    if not top_level_modules:
        err_print("error: circular dependency between modules detected\n")

    return build_subtree_list, top_level_modules


def add_build_subtree(modules_name_list, build_subtree_list, top_level_list,
                      modules_dict, recursive_level):
    for module_name in modules_name_list:
        if module_name in build_subtree_list:  # already seen
            if module_name in top_level_list:
                top_level_list.remove(
                    module_name
                )  # is a dependency of another module - not top-level
            continue

        if module_name not in modules_dict:
            continue
        module_info = modules_dict[module_name]
        if "depends" in module_info:
            if recursive_level < len(modules_dict):
                add_build_subtree(module_info["depends"], build_subtree_list,
                                  top_level_list, modules_dict,
                                  recursive_level + 1)
            else:
                top_level_list.clear()
                return

        build_subtree_list.append(module_name)


def order_modules_list_by_subtree(modules_name_list, build_subtree_list):
    ordered_name_list = []

    for module_name in build_subtree_list:
        if module_name in modules_name_list:
            ordered_name_list.append(module_name)

    return ordered_name_list


def create_tree_build_info_dict(modules_dict, modules_name_list,
                                single_modules_list, tree_build_targets,
                                cmake_generator, cmake_defines_dict,
                                binary_dir_path, build_type):
    build_info = {}
    signature = {}
    signature["build-type"] = build_type
    build_info["signature"] = signature
    build_info["build-targets"] = tree_build_targets
    cmake_info = {}
    cmake_info["generator"] = cmake_generator
    cmake_info["cmake-defines"] = create_build_info_cmake_defines_list(
        cmake_defines_dict, build_info_exclude_defines_list)
    build_info["cmake"] = cmake_info

    modules = {}
    for module_name in modules_name_list:
        cached_module_build_info = get_cached_module_build_info(module_name)
        module_build_info = {}
        module_build_info["signature"] = cached_module_build_info["signature"]
        module_build_info["cmake"] = cached_module_build_info["cmake"]
        modules[module_name] = module_build_info

    build_info["modules"] = modules

    dependencies = {}
    for dep_name in single_modules_list:
        cached_dep_build_info = get_cached_module_build_info(dep_name)
        dep_build_info = {}
        dep_build_info["signature"] = cached_dep_build_info["signature"]
        dependencies[dep_name] = dep_build_info

    build_info["dependencies"] = dependencies

    # generate build-id (signature hash) from everything except the build-id itself
    build_info["signature"]["build-id"] = build_info_hash_str(build_info)

    return build_info


def generate_tree_build_info_yaml(modules_dict, modules_name_list,
                                  single_modules_list, tree_build_targets,
                                  cmake_generator, cmake_defines_dict,
                                  binary_dir_path, build_type, dev_mode):

    del cmake_defines_dict["-DPYTHON_DEV_MODE"]

    generate_build_info_yaml_for_each_module(modules_dict, modules_name_list,
                                             cmake_generator,
                                             cmake_defines_dict,
                                             binary_dir_path)

    common_defines_list = [
        "-DCMAKE_RUNNER_DIR=" + root_dir + "/cmake-runner",
        "-DCMAKE_PREFIX_PATH=" + project_section["install-prefix"],
        "-DCMAKE_BUILD_TYPE=" + cmake_section["build-type"],
        "-DSINGLE_TREE_MODULES='" + (';'.join(modules_name_list) + "'"),
        "-DBUILD_ROOT_DIR=" + root_dir,
        "-DPYTHON_DEV_MODE=" + ("ON" if dev_mode else "OFF")
    ]

    # all keys must be present, no checks (force:True)
    add_user_defines(cmake_defines_dict, common_defines_list, True)

    tree_build_info = create_tree_build_info_dict(
        modules_dict, modules_name_list, single_modules_list,
        tree_build_targets, cmake_generator, cmake_defines_dict,
        binary_dir_path, build_type)

    if os.path.isdir(binary_dir_path):
        remove_files_by_pattern(binary_dir_path + "/init-" + build_type +
                                "-*.yaml")
    else:
        os.makedirs(binary_dir_path)

    signature_str = tree_build_info_to_signature_string(tree_build_info)
    tree_build_info_yaml = binary_dir_path + '/' + "init-" + signature_str + ".yaml"
    save_yaml(tree_build_info_yaml, tree_build_info)  # not saved in cache

    return signature_str


def get_module_custom_steps_list(phase_name, module_name, module_info):
    if phase_name in module_info:
        return module_info[phase_name]
        # full_module_name = modules_info["full_name"]
        #for step_exe in module_info[phase_name]:
        # steps_list.append(full_module_name + "/" + step_exe)
    else:
        return []


def aggregate_custom_steps_list(phase_name, modules_dict, modules_name_list):
    aggr_steps_list = []

    for module_name in modules_name_list:
        module_info = modules_dict[module_name]

        module_steps_list = get_module_custom_steps_list(
            phase_name, module_name, module_info)
        if module_steps_list:
            aggr_steps_list += module_steps_list

    return aggr_steps_list


def print_custom_steps(module_name, phase_name, steps_list):
    if steps_list:
        print("\t@echo \"\\n*** %s %s start ***\"" % (module_name, phase_name))
        for step_exe in steps_list:
            print("\t@echo \"\\n*** %s step: %s ***\"" %
                  (phase_name, step_exe))
            print("\t@%s" % (step_exe))
        print("\t@echo \"\\n*** %s %s done ***\"" % (module_name, phase_name))


def create_tree_make_target_deps_str(modules_dict, tree_modules_name_list):
    dependency_str = ""

    for module_name in tree_modules_name_list:
        module_info = modules_dict[module_name]

        if "depends" in module_info:
            for dep in module_info["depends"]:
                if dep not in tree_modules_name_list:
                    dependency_str += dep + "-install "

    return dependency_str


def generate_build_info_yaml_for_each_module(modules_dict, modules_name_list,
                                             cmake_generator,
                                             cmake_defines_dict,
                                             binary_dir_path):

    for module_name in modules_name_list:
        module_info = modules_dict[module_name]

        defines_dict = cmake_defines_dict.copy()

        if "cmake-defines" in module_info:
            add_user_defines(defines_dict, module_info["cmake-defines"])

        if "cmake" in module_info:
            lang_list, lang_defs = configure_languages(module_info["cmake"])
            if len(lang_list) > 0:
                add_user_defines(defines_dict, lang_defs)

        generate_module_build_info_yaml(module_name, module_info,
                                        cmake_generator, defines_dict, [],
                                        False,
                                        binary_dir_path + "/" + module_name,
                                        False)


def is_dev_mode(cmake_defines_dict):
    dev_mode = False
    if "-DPYTHON_DEV_MODE" in cmake_defines_dict:
        dev_mode_str = cmake_defines_dict["-DPYTHON_DEV_MODE"]
        dev_mode_upper = dev_mode_str.upper()
        if (dev_mode_upper == "ON" or dev_mode_upper == "YES"
                or dev_mode_upper == "TRUE" or dev_mode_upper == "Y"
                or dev_mode_upper == "1"):
            dev_mode = True

    return dev_mode


def generate_tree_build_makefile(manifest_root, modules_dict,
                                 tree_modules_list, single_modules_list,
                                 cmake_generator, cmake_defines_dict,
                                 cmake_build_opts, user_targets, project_env,
                                 nodeps, noprebuild, nopostinstall, force):

    tree_modules_str = ', '.join(tree_modules_list)
    print("### single tree build Makefile for: %s ###" % (tree_modules_str))

    print("\nroot_dir := %s" % (root_dir))
    print("script_dir := %s\n" % (script_dir))

    single_modules_force = False
    single_tree = True
    dev_mode = is_dev_mode(cmake_defines_dict)

    generate_module_build_makefile(
        modules_dict,
        single_modules_list,
        [],  # req_modules_name_list is replaced by the tree modules set
        [],  # top level modules are in req_modules_name_list, in the tree set
        cmake_defines_dict,
        cmake_generator,
        cmake_build_opts,
        ["all"],  # if module has no 'build-targets' and no targets on cmd line
        None,  # overrides 'build-targets' in module
        [],  # for which targets should we override
        project_env,  # environment variables for the project scope
        nodeps,  # use only installed products for dependencies, don't build them
        noprebuild,
        nopostinstall,
        single_modules_force,
        single_tree,
        dev_mode)

    build_type_lower = manifest_root["cmake"]["build-type"].lower()
    binary_dir_path = root_dir + "/cmake-build-" + build_type_lower

    signature = generate_tree_build_info_yaml(modules_dict, tree_modules_list,
                                              single_modules_list,
                                              user_targets, cmake_generator,
                                              cmake_defines_dict,
                                              binary_dir_path,
                                              build_type_lower, dev_mode)

    print("\n### targets for single tree (%s) ###\n" % (tree_modules_str))

    print("binary_dir := %s" % (binary_dir_path))
    print("signature := %s" % (signature))
    print("install_dir := %s" % (manifest_root["project"]["install-prefix"]))
    print("build_type := %s" % (cmake_section["build-type"].lower()))
    print("init_yaml := $(binary_dir)/init-$(signature).yaml")
    print("cmake_yaml := $(binary_dir)/cmake-$(signature).yaml")
    print("build_yaml := $(binary_dir)/build-$(signature).yaml")
    print("install_yaml := $(install_dir)/install-$(signature).yaml")
    print("gen_cmake_list := $(binary_dir)/CMakeLists-$(signature).txt")
    print("root_cmake_list := $(root_dir)/CMakeLists.txt")

    if not force:
        print("\n$(gen_cmake_list): | $(init_yaml)")
        print("\t@echo \"\\n*** tree generation: $(signature) ***\"\n")
    else:
        print("\n.PHONY: gen_cmake_list_force")
        print("$(gen_cmake_list): gen_cmake_list_force")
        print("gen_cmake_list_force:")
        print("\t@echo \"\\n*** tree force generation: $(signature) ***\"\n")

    print("\t@rm -f $(binary_dir)/CMakeLists-*.txt")

    cmake_defines_str = ""
    if cmake_defines_dict:
        for key, value in cmake_defines_dict.items():
            # remove -D from the key
            cmake_defines_str += "--cmake %s=%s " % (key[2:], value)

    print(
        "\t@python3 $(script_dir)/project.py -d $(script_dir) --root_dir $(root_dir) " \
        "tree generate-cmake " \
        "--dest_file $(gen_cmake_list) " \
        "--modules %s %s"
        % (' '.join(tree_modules_list), cmake_defines_str))

    print("\n.PHONY: gen_tree_list")
    print("gen_tree_list: | $(gen_cmake_list)")
    print("\t@ln -snf $(gen_cmake_list) $(root_cmake_list)")
    print("\t@echo \"\\n*** tree (%s): $(root_cmake_list)\"" %
          (tree_modules_str))

    # cmake
    if not force:
        print("\ncmake: $(cmake_yaml)")
        print("\t@echo \"\\n*** tree (%s) cmake done ***\"" %
              (tree_modules_str))

        dependency_str = create_tree_make_target_deps_str(
            modules_dict, tree_modules_list)

        if dependency_str:
            print("$(cmake_yaml): | %s gen_tree_list" % (dependency_str))
        else:
            print("$(cmake_yaml): | gen_tree_list")

        print("\t@echo \"\\n*** tree (%s) cmake start ***\"" %
              (tree_modules_str))
    else:
        print("\ncmake: cmake_force")
        print("\t@echo \"\\n*** tree (%s) forced cmake done ***\"" %
              (tree_modules_str))

        print("\n.PHONY: cmake_force")
        print("cmake_force: | gen_tree_list")
        print("\t@echo \"\\n*** tree (%s) forced cmake start ***\"" %
              (tree_modules_str))

    print("\t@rm -f $(binary_dir)/cmake-$(build_type)-*.yaml")

    exclude_gen = "Ninja" if cmake_generator == "Unix Makefiles" else "Unix Makefiles"

    print(
        "\t@if [ -f $(binary_dir)/CMakeCache.txt ] && grep -q \"%s\" $(binary_dir)/CMakeCache.txt; then rm -rf $(binary_dir); fi"
        % (exclude_gen))

    print("\tcmake -S . -B $(binary_dir) -G %s" % (cmake_generator))
    print("\tcp $(init_yaml) $(cmake_yaml)")

    # build
    if not force:
        print("\nbuild: $(build_yaml)")
        print("\t@echo \"\\n*** tree (%s) build done ***\"" %
              (tree_modules_str))

        print("$(build_yaml): | cmake")
        print("\t@echo \"\\n*** tree (%s) build start ***\"" %
              (tree_modules_str))
    else:
        # forced build
        print("\nbuild: build_force")
        print("\t@echo \"\\n*** tree (%s) forced build done ***\"" %
              (tree_modules_str))

        print("\n.PHONY: build_force")
        print("build_force: | cmake")
        print("\t@echo \"\\n*** tree (%s) forced build start ***\"" %
              (tree_modules_str))

    if not noprebuild:
        pre_build_list = aggregate_custom_steps_list("pre-build", modules_dict,
                                                     tree_modules_list)
        print_custom_steps("tree", "pre-build", pre_build_list)

    print("\t@rm -f $(binary_dir)/build-$(build_type)-*.yaml")

    if force:
        print("\tcmake --build $(binary_dir) -t clean -- %s" %
              (cmake_build_opts))

    if user_targets:
        for tgt in user_targets:
            print("\tcmake --build $(binary_dir) -t %s -- %s" %
                  (tgt, cmake_build_opts))
    else:
        print("\tcmake --build $(binary_dir) -- %s" % (cmake_build_opts))

    print("\tcp $(cmake_yaml) $(build_yaml)")

    # clean

    print("\nclean:")
    print("\t@echo \"\\n*** tree clean start ***\"")
    print("\trm -f $(binary_dir)/build-$(build_type)-*.yaml")
    print("\t[ -d $(binary_dir) ] && cmake --build $(binary_dir) -t clean")
    print("\t@echo \"\\n*** tree clean done ***\"")

    # install

    if not force:
        print("\ninstall: $(install_yaml)")
    else:
        print("\ninstall: install_force")

    print("\t@echo \"\\n*** tree install done ***\"")

    if not force:
        print("$(install_yaml): | build")
    else:
        print(".PHONY: install_force")
        print("\ninstall_force: | build")

    print("\t@echo \"\\n*** tree install start ***\"")

    print("\t@rm -f $(install_dir)/install-$(build_type)-*.yaml")
    print("\tcmake --install $(binary_dir)")
    print("\tcp $(build_yaml) $(install_yaml)")

    if not nopostinstall:
        post_install_list = aggregate_custom_steps_list(
            "post-install", modules_dict, tree_modules_list)
        print_custom_steps("tree", "post-install", post_install_list)


def generate_module_build_makefile(
        modules_dict,
        modules_name_list,
        req_modules_name_list,
        top_level_modules,
        cmake_defines_dict,
        cmake_generator,
        cmake_build_opts,
        default_cmake_build_tgts_list,  # if module has no 'build-targets' and no targets on cmd line
        special_cmake_build_tgts_list,  # overrides 'build-targets' in module
        special_tgt_names_list,  # for which targets should we override
        project_env,  # environment variables for the project scope
        nodeps,  # use only installed products for dependencies, don't build them
        noprebuild,
        nopostinstall,
        force,
        single_tree,
        dev_mode):

    if not isinstance(special_tgt_names_list, list):
        err_print("BUG: special_tgt_names_list is not of list type")

    if not isinstance(default_cmake_build_tgts_list, list):
        err_print("BUG: default_cmake_build_tgts_list is not of list type")

    print(".PHONY: all cmake build install clean pack\n")

    if modules_name_list:
        single_modules_str = ', '.join(modules_name_list)
        print("### targets for module build of: %s ###\n" %
              (single_modules_str))

        print("modules := %s" % (' '.join(modules_name_list)))
        print("tgts-cmake := $(addsuffix -cmake,$(modules))")
        print("tgts-build := $(addsuffix -build,$(modules))")
        print("tgts-clean := $(addsuffix -clean,$(modules))")
        print("tgts-install := $(addsuffix -install,$(modules))")
        print("tgts-pack := $(addsuffix -pack,$(modules))\n")

        print(
            ".PHONY: $(tgts-cmake) $(tgts-build) $(tgts-install) $(tgts-clean) $(tgts-pack)\n"
        )

        if not single_tree:
            print("cmake: %s" % ('-cmake '.join(top_level_modules) + '-cmake'))
            print("build: %s" % ('-build '.join(top_level_modules) + '-build'))
            print("clean: $(tgts-clean)")
            print("install: %s" %
                  ('-install '.join(top_level_modules) + '-install'))
            print("pack: $(tgts-pack)")

            print("\nall: build\n")

    print("export PYTHON_DEV_MODE=" + str(dev_mode))

    print_project_vars(project_env, "all")
    print_module_vars(modules_dict, "all", "project")

    print_project_vars(project_env, "install")
    print_module_vars(modules_dict, "install", "project")

    packer = root_dir + "/pack-runner/tarpack.py -q"
    print("\npacker := %s" % (packer))

    for module_name in modules_name_list:
        module_info = modules_dict[module_name]

        full_module_name = module_info["full_name"]
        module_path = root_dir + "/" + full_module_name

        var_module_name = module_info["var_name"]

        # create a local dict copy to change some keys for this module
        defines_dict = cmake_defines_dict.copy()
        if "-DPYTHON_DEV_MODE" in defines_dict:
            del defines_dict["-DPYTHON_DEV_MODE"]

        defines_dict["-DCMAKE_BUILD_TYPE"] = module_info["build-type"]

        if "cmake-defines" in module_info:
            add_user_defines(defines_dict, module_info["cmake-defines"])

        if module_info["platform"] == "cmake-runner":
            defines_dict["-DBUILD_ROOT_DIR"] = root_dir
            # allow user to define an alternative CMAKE_RUNNER_DIR
            if "-DCMAKE_RUNNER_DIR" not in defines_dict:
                defines_dict["-DCMAKE_RUNNER_DIR"] = root_dir + "/cmake-runner"

        if "install-prefix" in module_info:
            defines_dict["-DCMAKE_INSTALL_PREFIX"] = module_info[
                "install-prefix"]

        cmake_opts_str = "-G \"" + cmake_generator + "\""

        for key, value in defines_dict.items():
            cmake_opts_str += ' ' + key + '=' + str(value)

        depends_str = ''
        if "depends" in module_info:
            module_depends = module_info["depends"]
            if module_depends:
                depends_str = '-install '.join(module_depends) + "-install"

        src_dir_path = module_path
        if "root" in module_info:
            src_dir_path += "/" + module_info["root"]

        binary_dir_path = module_path + "/" + module_info["binary-dir"]

        install_prefix = defines_dict["-DCMAKE_INSTALL_PREFIX"]
        full_install_path = install_prefix + "/" + module_name

        installed_only = nodeps and module_name not in top_level_modules

        signature_prefix = module_info["build-type-lower"] + "-commit"

        tar_out_dir = module_info["pack"]["out-dir"]

        if module_name in special_tgt_names_list:
            cmake_build_tgts_list = special_cmake_build_tgts_list
        elif "build-targets" in module_info:
            cmake_build_tgts_list = module_info["build-targets"]
        else:
            cmake_build_tgts_list = default_cmake_build_tgts_list

        signature_id = generate_module_build_info_yaml(
            module_name, module_info, cmake_generator, defines_dict,
            cmake_build_tgts_list, installed_only, binary_dir_path,
            install_prefix)

        # env vars
        print()
        print_module_vars(modules_dict, module_name, "module")

        # make vars
        print("%s_signature := %s" % (var_module_name, signature_id))

        if installed_only:
            if depends_str:
                print("\n%s-install: | %s" % (module_name, depends_str))
            else:
                print("\n%s-install:" % (module_name))
            print(
                "\t@echo \"\\n*** %s install: using pre-installed products, nothing to do ***\\n*** using: $(%s_signature)\""
                % (module_name, var_module_name))
            continue

        print("%s_source_dir := %s" % (var_module_name, src_dir_path))
        print("%s_binary_dir := %s" % (var_module_name, binary_dir_path))
        print("%s_init_yaml := $(%s_binary_dir)/init-$(%s_signature).yaml" %
              (var_module_name, var_module_name, var_module_name))
        print("%s_cmake_yaml := $(%s_binary_dir)/cmake-$(%s_signature).yaml" %
              (var_module_name, var_module_name, var_module_name))
        print("%s_build_yaml := $(%s_binary_dir)/build-$(%s_signature).yaml" %
              (var_module_name, var_module_name, var_module_name))

        print("%s_install_prefix := %s" % (var_module_name, install_prefix))
        print("%s_install_dir := %s" % (var_module_name, full_install_path))
        print(
            "%s_install_yaml := $(%s_install_dir)/install-$(%s_signature).yaml"
            % (var_module_name, var_module_name, var_module_name))

        print("%s_tar_src_dir := /tmp/pack-src-$(%s_signature)" %
              (var_module_name, var_module_name))
        print("%s_tar_out_dir := %s" % (var_module_name, tar_out_dir))

        print("%s_tar_file := $(%s_tar_out_dir)/%s-pack-$(%s_signature).tgz" %
              (var_module_name, var_module_name, var_module_name,
               var_module_name))
        print()

        module_force = force if module_name in req_modules_name_list else False

        # cmake generate and configure
        if not module_force:
            print("%s-cmake: $(%s_cmake_yaml)" %
                  (module_name, var_module_name))
        else:
            print("%s-cmake: %s_cmake_force" % (module_name, module_name))

        print("\t@echo \"\\n*** %s cmake done ***\"" % (module_name))

        if not module_force:
            if depends_str:
                print("$(%s_cmake_yaml): | %s" %
                      (var_module_name, depends_str))
            else:
                print("$(%s_cmake_yaml):" % (var_module_name))

        else:
            print("\n.PHONY: %s_cmake_force" % (module_name))
            if depends_str:
                print("%s_cmake_force: | %s" % (module_name, depends_str))
            else:
                print("%s_cmake_force:" % (module_name))

        print("\t@echo \"\\n*** %s cmake start ***\"" % (module_name))

        print("\t@rm -f $(%s_binary_dir)/cmake-%s*.yaml" %
              (var_module_name, signature_prefix))

        if not noprebuild and "pre-build" in module_info:
            pre_build_list = get_module_custom_steps_list(
                "pre-build", module_name, module_info)
            print_custom_steps(module_name, "pre-build", pre_build_list)

        exclude_gen = "Ninja" if cmake_generator == "Unix Makefiles" else "Unix Makefiles"
        print(
            "\t@if [ -f $(%s_binary_dir)/CMakeCache.txt ] && grep -q \"%s\" $(%s_binary_dir)/CMakeCache.txt; then rm -rf $(%s_binary_dir); fi"
            % (var_module_name, exclude_gen, var_module_name, var_module_name))

        print("\tcmake -S $(%s_source_dir) -B $(%s_binary_dir) %s" %
              (var_module_name, var_module_name, cmake_opts_str))

        print("\tcp $(%s_init_yaml) $(%s_cmake_yaml)" %
              (var_module_name, var_module_name))

        # build

        if not module_force:
            print("\n%s-build: $(%s_build_yaml)" %
                  (module_name, var_module_name))
            print("\t@echo \"\\n*** %s build done ***\"" % (module_name))

            print("$(%s_build_yaml): | %s-cmake" %
                  (var_module_name, module_name))
            print("\t@echo \"\\n*** %s build start ***\"" % (module_name))
        else:
            # forced build
            print("\n%s-build: %s_build_force" % (module_name, module_name))
            print("\t@echo \"\\n*** %s forced build done ***\"" %
                  (module_name))

            print("\n.PHONY: %s_build_force" % (module_name))
            print("%s_build_force: | %s-cmake" % (module_name, module_name))
            print("\t@echo \"\\n*** %s forced build start ***\"" %
                  (module_name))

        print("\t@rm -f $(%s_binary_dir)/build-%s*.yaml" %
              (var_module_name, signature_prefix))

        if module_force:
            print("\tcmake --build $(%s_binary_dir) -t clean -- %s" %
                  (var_module_name, cmake_build_opts))

        for target in cmake_build_tgts_list:
            print("\tcmake --build $(%s_binary_dir) -t %s -- %s" %
                  (var_module_name, target, cmake_build_opts))

        print("\tcp $(%s_cmake_yaml) $(%s_build_yaml)" %
              (var_module_name, var_module_name))

        # clean

        print("\n%s-clean:" % (module_name))
        print("\t@echo \"\\n*** %s clean ***\\n\"" % (module_name))
        print("\trm -f $(%s_binary_dir)/build-*.yaml" % (var_module_name))
        print(
            "\t[ -d $(%s_binary_dir) ] && cmake --build $(%s_binary_dir) -t clean"
            % (var_module_name, var_module_name))

        # install

        if not module_info["install"]:
            print("\t@echo \"\\n*** %s install disabled, nothing to do ***\"" %
                  (module_name))
            return

        install_signature_yaml = full_install_path + "/install-" + signature_id + ".yaml"
        tar_file = tar_out_dir + "/" + module_name + "-pack-" + signature_id + ".tgz"

        if module_force:
            cmd = InstallOp.DO_INSTALL
        elif os.path.isfile(install_signature_yaml):
            cmd = InstallOp.DO_NOTHING
        elif os.path.isfile(tar_file):
            cmd = InstallOp.DO_UNPACK
        else:
            cmd = InstallOp.DO_INSTALL

        if cmd == InstallOp.DO_NOTHING:
            if depends_str:
                print("\n%s-install: | %s" % (module_name, depends_str))
            else:
                print("\n%s-install:" % (module_name))

            print("\t@echo \"\\n*** %s install - nothing to do ***\"" %
                  (module_name))

        elif cmd == InstallOp.DO_UNPACK:
            if depends_str:
                print("\n%s-install: | %s" % (module_name, depends_str))
            else:
                print("\n%s-install:" % (module_name))

            print("\t@echo \"\\n*** %s install - unpack tar ***\"" %
                  (module_name))

            print("\t@mkdir -p $(%s_install_prefix)" % (var_module_name))
            print("\t@rm -f $(%s_install_dir)/install-%s*.yaml" %
                  (var_module_name, signature_prefix))

            print(
                "\t@if [ -d $(%s_install_dir) ]; then echo '\\n*** %s removing previous installation ***' && $(packer) purge -d $(%s_install_dir) -y tarpack-%s.yaml  && rm -rf $(%s_install_dir); fi"
                % (var_module_name, module_name, var_module_name,
                   var_module_name, var_module_name))

            print(
                "\t$(packer) unpack -t $(%s_tar_file) -o $(%s_install_prefix)"
                % (var_module_name, var_module_name))

        elif cmd == InstallOp.DO_INSTALL:
            if not module_force:
                print("\n%s-install: $(%s_install_yaml)" %
                      (module_name, var_module_name))
                print("\t@echo \"\\n*** %s install done ***\"" % (module_name))

                print("$(%s_install_yaml): | %s-build" %
                      (var_module_name, module_name))
                print("\t@echo \"\\n*** %s install start ***\"" %
                      (module_name))
            else:
                print("\n%s-install: %s_install_force" %
                      (module_name, module_name))
                print("\t@echo \"\\n*** %s forced install done ***\"" %
                      (module_name))

                print(".PHONY: %s_install_force" % (module_name))
                print("\n%s_install_force: | %s-build" %
                      (module_name, module_name))
                print("\t@echo \"\\n*** %s forced install start ***\"" %
                      (module_name))

            print("\t@rm -f $(%s_install_dir)/install-%s*.yaml" %
                  (var_module_name, signature_prefix))
            print("\tcmake --install $(%s_binary_dir)" % (var_module_name))
            print("\t@mkdir -p $(%s_install_dir)" % (var_module_name))
            print("\tcp $(%s_build_yaml) $(%s_install_yaml)" %
                  (var_module_name, var_module_name))

        if not nopostinstall and "post-install" in module_info and cmd != InstallOp.DO_NOTHING:
            post_install_list = get_module_custom_steps_list(
                "post-install", module_name, module_info)
            print_custom_steps(module_name, "post-install", post_install_list)

        # pack

        print("\n%s-pack: $(%s_tar_file)" % (module_name, var_module_name))
        print("\t@echo \"\\n*** %s pack done ***\"" % (module_name))
        if os.path.isfile(tar_file):
            print("$(%s_tar_file):" % (var_module_name))
        else:
            print("$(%s_tar_file): | %s-install" %
                  (var_module_name, module_name))
            print("\t@echo \"\\n*** %s pack start ***\"" % (module_name))

            print("\t@mkdir -p $(%s_tar_out_dir)" % (var_module_name))
            if "tags" in module_info["pack"]:
                packer += " --tag " + ' '.join(module_info["pack"]["tags"])

            yaml_dir_files_str = ""

            if "yamls" in module_info["pack"]:
                file_dict = {}
                for yml in module_info["pack"]["yamls"]:
                    yaml_file = yml if os.path.isabs(
                        yml
                    ) else install_prefix + "/" + module_name + "/" + yml
                    head_tail = os.path.split(yaml_file)
                    if head_tail[0] not in file_dict:
                        file_dict[head_tail[0]] = []
                    file_dict[head_tail[0]].append(head_tail[1])

                for dir_key, path_value in file_dict.items():
                    yaml_dir_files_str += " --dir " + dir_key
                    yaml_dir_files_str += " --yaml " + " --yaml ".join(
                        path_value)
            else:
                pack_yaml_file = "tarpack-" + module_name + ".yaml"
                yaml_dir_files_str = "--dir $(%s_install_dir) --yaml %s" % (
                    var_module_name, pack_yaml_file)

            print(
                "\t$(packer) pack %s --out $(%s_tar_file) --tar-dir $(%s_tar_src_dir)"
                % (yaml_dir_files_str, var_module_name, var_module_name))


def generate_module_unpack_makefile(project_section, git_modules, modules,
                                    nopostinstall, unpack_file):
    module_name = modules[0]
    print("module := %s" % (module_name))
    print("tgts-unpack := $(addsuffix -unpack,$(module))\n")
    print("unpack: $(tgts-unpack)")
    print("\nall: unpack\n")
    print(".PHONY: all unpack $(tgts-unpack)\n")

    module_info = git_modules[module_name]
    install_prefix = module_info[
        "install-prefix"] if "install-prefix" in module_info else project_section[
            "install-prefix"]
    full_install_path = install_prefix + "/" + module_name

    packer = root_dir + "/pack-runner/tarpack.py -q"

    print("\npacker := %s" % (packer))
    print("%s_install_dir := %s" % (module_name, full_install_path))
    print("%s_install_prefix := %s" % (module_name, install_prefix))

    print("\n%s-unpack:" % (module_name))
    print(
        "\t@if [ -d $(%s_install_dir) ]; then echo '\\n*** %s removing previous installation ***' && $(packer) purge -d $(%s_install_dir) -y tarpack-%s.yaml  && rm -rf $(%s_install_dir); fi"
        % (module_name, module_name, module_name, module_name, module_name))

    print("\t@echo \"\\n*** %s unpack start ***\"" % (module_name))
    print("\t$(packer) unpack -t %s -o $(%s_install_prefix)" %
          (unpack_file, module_name))

    if not nopostinstall and "post-install" in module_info:
        post_install_list = get_module_custom_steps_list(
            "post-install", module_name, module_info)
        print_custom_steps(module_name, "post-install", post_install_list)


def generate_tree_purge_makefile(manifest_root, install_path, build_path):
    print(".PHONY: purge\n")
    print("purge:")
    if install_path:
        print("\t@echo \"\\nremoving tree install ...\"")
        print("\trm -rf %s" % (manifest_root["project"]["install-prefix"]))

    if build_path:
        print("\t@echo \"\\nremoving binaries ...\"")
        print("\trm -rf %s/cmake-build-%s" %
              (root_dir, manifest_root["cmake"]["build-type"].lower()))


def generate_single_module_purge_make_targets(packer, module_name,
                                              full_module_name, install_path,
                                              build_path, tar_out_dir):

    print("%s-purge:" % (module_name))
    if install_path:
        print("\t@echo \"\\nremoving %s install ...\"" % (module_name))
        module_install_path = install_path + "/" + full_module_name
        print(
            "\t@if [ -d \"%s\" ]; then if [ -f \"%s/tarpack-%s.yaml\" ]; then echo \"purge using tarpack-%s.yaml...\"; %s purge -d %s -y tarpack-%s.yaml; fi && rm -rf %s; fi"
            % (module_install_path, module_install_path, module_name,
               module_name, packer, module_install_path, module_name,
               module_install_path))

    if build_path:
        print("\t@echo \"\\nremoving %s binaries ...\"" % (module_name))
        print("\trm -rf %s" % (build_path))

    if tar_out_dir:
        print("\t@echo \"\\nremoving %s tars ...\"" % (module_name))
        print("\trm -f %s/%s*" % (tar_out_dir, module_name))


def generate_module_purge_makefile(modules_dict, modules_name_list,
                                   pack_options, install, binary_dir,
                                   tar_file):
    print("modules := %s" % (' '.join(modules_name_list)))
    print("tgts-purge := $(addsuffix -purge,$(modules))")
    print("purge: $(tgts-purge)\n")
    print(".PHONY: purge $(tgts-purge)")

    packer = root_dir + "/pack-runner/tarpack.py"
    if pack_options:
        packer += " " + pack_options

    for module_name in modules_name_list:
        module_info = modules_dict[module_name]

        full_module_name = module_info["full_name"]

        if install:
            if "install-prefix" in module_info:
                install_path = module_info["install-prefix"]
            else:
                install_path = project_section["install-prefix"]
        else:
            install_path = None

        if binary_dir:
            build_path = full_module_name + "/" + module_info["binary-dir"]
        else:
            build_path = None

        if tar_file:
            tar_out_dir = module_info["pack"]["out-dir"]
        else:
            tar_out_dir = None

        generate_single_module_purge_make_targets(packer, module_name,
                                                  full_module_name,
                                                  install_path, build_path,
                                                  tar_out_dir)


def git_head_commit_id_str(module_path):
    commit_id = subprocess.check_output(
        ['git', '-C', module_path, 'rev-parse', 'HEAD'])
    commit_id = str(commit_id, "utf-8").strip()
    return commit_id


def git_diff_hash_str(module_path):
    diff_text = subprocess.check_output(
        ['git', '-C', module_path, 'diff', 'HEAD'])
    if not diff_text:
        return None

    h = hashlib.md5(diff_text)
    return h.hexdigest()


def build_info_hash_str(build_info):
    h = hashlib.md5(yaml.dump(build_info, sort_keys=False).encode('utf-8'))
    return h.hexdigest()


# def build_info_yaml_hash_str(build_info_yaml):

#     if not os.path.isfile(build_info_yaml):
#         err_print("ERROR: %s not found" % (build_info_yaml))

#     h = hashlib.md5()

#     with open(build_info_yaml, 'rb') as file:
#         while True:
#             # Reading is buffered, so we can read smaller chunks.
#             chunk = file.read(h.block_size)
#             if not chunk:
#                 break
#             h.update(chunk)

#     return h.hexdigest()


def probe_file(dir_path, file_name):
    return os.path.isfile(dir_path + "/" + file_name)


def find_tarpack_yaml(install_path, module_name, pack_yaml_file=None):
    if not pack_yaml_file:
        pack_yaml_file = "tarpack-" + module_name + ".yaml"

    if probe_file(install_path, pack_yaml_file):
        return True, install_path, pack_yaml_file
    else:
        install_path += "/" + module_name
        if probe_file(install_path, pack_yaml_file):
            return True, install_path, pack_yaml_file
        else:
            return False, "", ""


def generate_tree_root_cmake(cmake_modules, modules_name_list,
                             cmake_defines_dict, project_section,
                             cmake_section, lang_list, lang_defs, cmake_file):

    out = "cmake_minimum_required(VERSION " + cmake_section[
        "min-version"] + " )\n\n"

    for key, value in lang_defs.items():
        out += "unset(" + key + " CACHE)\n" \
        "set(" + key + " " + str(value) + ")\n\n"

    langs = ' '.join(lang_list)
    out += "project( " + project_section[
        "name"] + " LANGUAGES " + langs + ")\n\n"

    out += "set(SINGLE_TREE ON)\n\n"

    for key, value in cmake_defines_dict.items():
        clean_key = key[2:]  # remove -D
        clean_key = clean_key.split(':', 1)[0]  # remove types like :BOOL
        if ';' in value:
            out += "set(%s \"%s\")\n" % (clean_key, value)
        else:
            out += "set(%s %s)\n" % (clean_key, value)

    out += "\nmessage(\"CMAKE_CXX_COMPILER=${CMAKE_CXX_COMPILER}\")\n"
    out += "message(\"CMAKE_C_COMPILER=${CMAKE_C_COMPILER}\")\n"

    out += "\nmessage(\"CMAKE_CXX_COMPILER_LAUNCHER=${CMAKE_CXX_COMPILER_LAUNCHER}\")\n"
    out += "message(\"CMAKE_C_COMPILER_LAUNCHER=${CMAKE_C_COMPILER_LAUNCHER}\")\n"

    out += "\nmessage(\"CMAKE_RUNNER_DIR=${CMAKE_RUNNER_DIR}\")\n"
    out += "include(${CMAKE_RUNNER_DIR}/include/cmake-runner-std.cmake)\n"

    add_module = "\n"

    for module_name in modules_name_list:
        module_info = cmake_modules[module_name]

        module_root = module_info["full_name"]
        if "root" in module_info:
            module_root += "/" + module_info["root"]

        func_module_name = module_name.replace("/", "__")
        out += "\nfunction(add_%s)\n" % (func_module_name)
        if "cmake-defines" in module_info and module_info["cmake-defines"]:
            for cmk_define in module_info["cmake-defines"]:
                key, value = cmk_define.split('=')
                clean_key = key.split(':', 1)[0]  # remove types like :BOOL
                out += "\tset(%s %s)\n" % (clean_key, value)

        if "cmake" in module_info:
            lang_list, lang_defs = configure_languages(module_info["cmake"])
            if len(lang_list) > 0:
                for key, value in lang_defs.items():
                    out += "\tset(%s %s)\n" % (key, value)

        out += "\tadd_subdirectory(%s)" % (module_root)
        out += "\nendfunction()\n"
        add_module = add_module + "add_" + func_module_name + "()\n"

    out += add_module

    if not cmake_file:
        print(out)
    else:
        with open(cmake_file, "w") as external_file:
            print(out, file=external_file)
            external_file.close()


def get_schema_name(manifest_root, schema_yaml=None):
    if not schema_yaml:
        if "format" not in manifest_root:
            err_print("'format' not found in {}".format(manifest_path))
        manifest_format = manifest_root["format"]

        if "version" not in manifest_format or not manifest_format[
                "version"]:  # check that format.version is present and get its value
            err_print("'version' not found in 'format' block of {}".format(
                manifest_path))

        if "type" not in manifest_format or not manifest_format[
                "type"]:  # check that format.type is present and get its value
            err_print("'type' not found in 'format' block of {}".format(
                manifest_path))

        format_ver = manifest_format["version"]
        schema_type = manifest_format["type"]
        schema_yaml = schema_type + "-" + format_ver + ".yaml"

    return script_dir + "/schema/" + schema_yaml


def tarpack_config(manifest_root, pack_modules, root_dir):
    if "pack" not in manifest_root:
        manifest_root["pack"] = {}

    if "out-dir" not in manifest_root[
            "pack"] or not manifest_root["pack"]["out-dir"]:
        manifest_root["pack"]["out-dir"] = "tarpack"  # default

    if not os.path.isabs(manifest_root["pack"]["out-dir"]):
        abs_out_path = root_dir + "/" + manifest_root["pack"]["out-dir"]
        manifest_root["pack"]["out-dir"] = abs_out_path

    proj_pack_conf = manifest_root["pack"]

    for name in pack_modules:
        module_conf = pack_modules[name]
        if "tags" in proj_pack_conf:
            if "pack" in module_conf:
                if "tags" in module_conf["pack"]:
                    module_unique_tags = set(proj_pack_conf["tags"] +
                                             module_conf["pack"]["tags"])
                    module_conf["pack"]["tags"] = list(module_unique_tags)

        if "pack" not in module_conf:
            module_conf["pack"] = proj_pack_conf
        elif "out-dir" not in module_conf["pack"]:
            module_conf["pack"]["out-dir"] = proj_pack_conf["out-dir"]
        elif not os.path.isabs(module_conf["pack"]["out-dir"]):
            abs_out_path = root_dir + "/" + module_conf["pack"]["out-dir"]
            module_conf["pack"]["out-dir"] = abs_out_path


def save_yaml(yaml_path, data):
    try:
        with open(yaml_path, 'w') as f:
            try:
                yaml.dump(data, f, sort_keys=False)
            except yaml.YAMLError as exc:
                pm = exc.problem_mark
                err_print(
                    "your file {} has an issue on line {} at position {}".
                    format(yaml_path, pm.line, pm.column))
            except Exception as e:
                err_print("save_yaml({}) failed. {}".format(yaml_path, str(e)))
    except IOError as e:
        err_print("save_yaml({}) failed. I/O error({0}): {1}".format(
            yaml_path, e.errno, e.strerror))
    except:  #handle other exceptions such as attribute errors
        print("save_yaml({}) failed. Unexpected error: {}".format(
            yaml_path,
            sys.exc_info()[0]))


def load_yaml(yaml_path):
    data = {}
    try:
        with open(yaml_path, "r") as file:
            try:
                data = yaml.load(file, Loader=yaml.FullLoader)
            except yaml.YAMLError as exc:
                pm = exc.problem_mark
                err_print(
                    "load_yaml({}) failed. your file has an issue on line {} at position {}"
                    .format(yaml_path, pm.line, pm.column))
            except Exception as e:
                err_print("load_yaml({}) failed. {}".format(yaml_path, str(e)))
    except FileNotFoundError:
        err_print("file {} does not exist".format(yaml_path))
    except IOError as e:
        err_print("load_yaml({}) failed. I/O error({}): {}".format(
            yaml_path, e.errno, e.strerror))
    except:
        err_print("load_yaml({}) failed. Unexpected error: {}".format(
            yaml_path,
            sys.exc_info()[0]))

    return data


def set_manifest_defaults(manifest_root):
    if "cmake" in manifest_root and "build-type" in manifest_root["cmake"]:
        module_info_defaults["build-type"] = manifest_root["cmake"][
            "build-type"]


def set_modules_defaults(modules_list):
    for name in modules_list:
        module_info = modules_list[name]

        for key in module_info_defaults:
            if key not in module_info:
                module_info[key] = module_info_defaults[key]

        # handle other dependencies
        if not module_info["build"]:
            module_info["install"] = False

        if "build-type" in module_info:
            module_info["build-type-lower"] = module_info["build-type"].lower()
            module_info["binary-dir"] = "cmake-build-" + module_info[
                "build-type-lower"]


def configure_exclude_list(exclude_list, cmake_section):
    if cmake_section and "exclude-from-signature" in cmake_section:
        for var in cmake_section["exclude-from-signature"]:
            if not var[0:2] == "-D":
                var = "-D" + var
            exclude_list.append(var)


def configure_languages(cmake_section):
    lang_list = []
    lang_defs = {}

    if "languages" in cmake_section:
        for lang_dict in cmake_section["languages"]:
            lang_name = lang_dict["name"]
            lang_list.append(lang_name)
            if "compiler" in lang_dict:
                compiler_def = "CMAKE_" + lang_name + "_COMPILER"
                lang_defs[compiler_def] = lang_dict["compiler"]
            if "launcher" in lang_dict:
                launcher_def = "CMAKE_" + lang_name + "_COMPILER_LAUNCHER"
                lang_defs[launcher_def] = lang_dict["launcher"]
            if "standard" in lang_dict:
                standard_def = "CMAKE_" + lang_name + "_STANDARD"
                lang_defs[standard_def] = lang_dict["standard"]

    return lang_list, lang_defs


def set_modules_refs(modules_dict, module_aliases_dict):
    for module_name, module in modules_dict.items():
        if "depends" in module:
            for dep_idx, dep_mod_name in enumerate(module["depends"]):
                if dep_mod_name not in modules_dict:
                    if dep_mod_name not in module_aliases_dict:
                        err_print(
                            "module %s depends on %s, which does not exist" %
                            (module_name, dep_mod_name))
                    real_module_name = module_aliases_dict[dep_mod_name]
                    module["depends"][dep_idx] = real_module_name


def get_build_opts(extra_args):
    if extra_args:
        cmake_build_opts = ' '.join(extra_args)
        regex = re.compile('^(-j|--parallel)\\s*\\d*$')
        parallel = [e_arg for e_arg in extra_args if regex.match(e_arg)]
        if not parallel:
            cmake_build_opts += " -j 8"
    else:
        cmake_build_opts = " -j 8"

    return cmake_build_opts


def load_manifest(manifest_path, schema_yaml=None):

    # load manifest yaml
    manifest_root = load_yaml(manifest_path)

    schema_path = get_schema_name(manifest_root, schema_yaml)

    # determine the schema file path and check it exists
    if not os.path.exists(schema_path):
        err_print("schema file {} does not exist".format(schema_path))

    # validate manifest yaml against the schema
    try:
        s = SchemaValidator(source_file=manifest_path,
                            schema_files=[schema_path])
        s.validate(raise_exception=True)
    except SchemaError as e:
        err_print("Manifest YAML validation failed")

    return manifest_root


# project.py main

if __name__ == "__main__":

    # initialize arg parser
    parser = argparse.ArgumentParser(description='manifest handling helper')

    parser.add_argument('-d',
                        '--script_dir',
                        required=True,
                        help="current script dir")

    parser.add_argument('-r', '--root_dir', required=True, help="root dir")

    parser.add_argument('-m',
                        '--manifest',
                        help='manifest path',
                        dest="manifest_path")

    parser.add_argument('-s',
                        '--schema',
                        help='schema file',
                        dest="schema_yaml")

    subparsers = parser.add_subparsers(dest='command')

    parser_git = subparsers.add_parser('git', help="git commands")
    parser_tree = subparsers.add_parser('tree', help="tree commands")
    parser_list = subparsers.add_parser('list', help="list comands")
    parser_module = subparsers.add_parser('module', help="module commands")
    parser_check = subparsers.add_parser('check', help="check commands")
    parser_get = subparsers.add_parser('get', help="check commands")

    parser_git.add_argument('mode',
                            choices=[
                                "sync", "reset", "fetch", "push", "status",
                                "diff", "pull", "check", "get"
                            ],
                            help='git subcommand')

    # git
    parser_git.add_argument('-c',
                            '--check',
                            action='store_true',
                            help='checking mode')

    parser_git.add_argument('-f',
                            '--force',
                            action='store_true',
                            help='proceed when errors')

    parser_git.add_argument('-w', '--flow', dest="flow", help='flow')

    parser_git.add_argument('-M',
                            '--modules',
                            metavar='module-list',
                            nargs='+',
                            help="list of module names")

    # tree
    parser_tree.add_argument(
        'mode',
        choices=["generate-cmake", "generate-make", "purge"],
        help='tree subcommand',
        default="generate_cmake")

    parser_tree.add_argument('-M',
                             '--modules',
                             metavar='module-list',
                             nargs='+',
                             help="list of module names")

    parser_tree.add_argument('-c',
                             '--cmake',
                             action='append',
                             help='CMake variables',
                             dest="cmake_vars")

    parser_tree.add_argument('-t',
                             '--target',
                             metavar='target-list',
                             nargs='+',
                             help="target for cmake --build")

    parser_tree.add_argument('-g',
                             '--gen',
                             choices=["make", "ninja"],
                             help='CMake generator type',
                             dest="generator")

    parser_tree.add_argument('-N',
                             '--nodeps',
                             action='store_true',
                             help='no dependencies build')

    parser_tree.add_argument('-P',
                             '--noprebuild',
                             action='store_true',
                             help='no prebuild step')

    parser_tree.add_argument('-I',
                             '--nopostinstall',
                             action='store_true',
                             help='no postinstall step')

    parser_tree.add_argument('-v',
                             '--verbose',
                             action='store_true',
                             help='VERBOSE mode enable')

    parser_tree.add_argument('-d',
                             '--dest_file',
                             dest='cmake_file_name',
                             help='cmake file name')

    parser_tree.add_argument('-i',
                             '--install-dir',
                             action='store_true',
                             help='purge install dir')

    parser_tree.add_argument('-b',
                             '--build-dir',
                             action='store_true',
                             help='purge build dir')

    parser_tree.add_argument('-f',
                             '--force',
                             action='store_true',
                             help='force operation')

    # list
    parser_list.add_argument('mode',
                             choices=["cmake", "git", "all"],
                             help='list subcommand',
                             default="cmake")

    parser_list.add_argument('-f',
                             '--full',
                             action='store_true',
                             help='full module info')

    # module
    parser_module.add_argument('mode',
                               choices=["build", "purge", "unpack"],
                               help='module subcommand',
                               default="build")

    parser_module.add_argument('-M',
                               '--modules',
                               metavar='module-list',
                               nargs='+',
                               help="list of module names")

    parser_module.add_argument('-t',
                               '--target',
                               metavar='target-list',
                               nargs='+',
                               help="target for cmake --build")

    parser_module.add_argument('-g',
                               '--gen',
                               choices=["make", "ninja"],
                               help='CMake generator type',
                               dest="generator")

    parser_module.add_argument('-N',
                               '--nodeps',
                               action='store_true',
                               help='no dependencies build')

    parser_module.add_argument('-P',
                               '--noprebuild',
                               action='store_true',
                               help='no prebuild step')

    parser_module.add_argument('-I',
                               '--nopostinstall',
                               action='store_true',
                               help='no postinstall step')

    parser_module.add_argument('-v',
                               '--verbose',
                               action='store_true',
                               help='VERBOSE mode enable')

    parser_module.add_argument('-q',
                               '--quiet',
                               action='store_true',
                               help='quiet mode enable')

    parser_module.add_argument('-a',
                               '--tool-args',
                               metavar='tool-args',
                               nargs='+',
                               help="list of extra command args for tools")

    parser_module.add_argument('-c',
                               '--cmake',
                               action='append',
                               help='CMake variables',
                               dest="cmake_vars")

    parser_module.add_argument('-i',
                               '--install-dir',
                               action='store_true',
                               help='purge install dir')

    parser_module.add_argument('-b',
                               '--build-dir',
                               action='store_true',
                               help='purge build dir')

    parser_module.add_argument('-p',
                               '--pack-files',
                               action='store_true',
                               help='purge tar files')

    parser_module.add_argument('-u',
                               '--unpack-file',
                               dest="unpack_file",
                               help='unpack file name')

    parser_module.add_argument('-f',
                               '--force',
                               action='store_true',
                               help='force operation')

    # check
    parser_check.add_argument('mode',
                              choices=["cmake", "git", "all"],
                              help='check subcommand',
                              default="cmake")

    parser_check.add_argument('check_modules_list',
                              nargs='+',
                              help="list of modules to check")

    # get
    parser_get.add_argument('mode',
                            choices=["property"],
                            help='property',
                            default="property")

    parser_get.add_argument('-l',
                            '--name-list',
                            metavar='name-list',
                            nargs='+',
                            help="list of module names")

    # parse the cmd line args
    (args, extra_args) = parser.parse_known_args()

    if hasattr(args, "tool_args") and args.tool_args:
        extra_args = [arg.split(":")[1] for arg in args.tool_args]

    script_dir = args.script_dir
    root_dir = os.path.abspath(args.root_dir)

    # configure yaml dumper
    yaml.Dumper.ignore_aliases = lambda *args: True

    manifest_path = args.manifest_path if args.manifest_path else root_dir + "/manifest.yaml"
    schema_yaml = args.schema_yaml

    command = args.command

    # load and check manifest
    manifest_root = load_manifest(manifest_path, schema_yaml)

    # set manifest project scope defaults
    set_manifest_defaults(manifest_root)

    # get manifest blocks
    #format_section = manifest_root["format"]
    cmake_section = manifest_root["cmake"] if "cmake" in manifest_root else {}
    project_section = manifest_root["project"]
    git_modules = manifest_root["modules"]  # all first-level modules

    # find all submodules and create a first-level dictionary with 'parent_module' set
    submodule_modules = {
        submodule_name: {
            **submodule_val,
            "parent_module": module_name,
            "full_name": module_name + "/" + submodule_name,
            "var_name": submodule_name.replace("/", "__"),
        }
        for module_name in git_modules
        if "submodules" in git_modules[module_name] for submodule_name,
        submodule_val in git_modules[module_name]["submodules"].items()
    }

    # set full_name to the module's name for all first-level modules
    # submodules has this field set during the construction step above
    for module_name, module_item in git_modules.items():
        module_item["full_name"] = module_name
        module_item["var_name"] = module_name.replace("/", "__")

    # if any submodules found, add them to the general modules list
    if submodule_modules:
        all_modules = git_modules.copy()
        all_modules.update(submodule_modules)
    else:
        all_modules = git_modules

    module_aliases_dict = {
        module["alias"]: module_name
        for module_name, module in all_modules.items() if "alias" in module
    }

    set_modules_refs(all_modules, module_aliases_dict)

    # set defaults for all modules including submodules
    set_modules_defaults(all_modules)

    # cmake modules subset of all build modules
    cmake_modules = {
        module_name: all_modules[module_name]
        for module_name in all_modules
        if (all_modules[module_name]["platform"] == "cmake"
            or all_modules[module_name]["platform"] == "cmake-runner")
        and all_modules[module_name]["build"]
    }

    # create lists of modules names
    git_module_names_list = [module_name for module_name in git_modules]
    all_module_names_list = [module_name for module_name in all_modules]
    build_module_names_list = [
        module_name for module_name in all_modules
        if all_modules[module_name]["build"]
    ]
    cmake_module_names_list = [module_name for module_name in cmake_modules]

    tarpack_config(manifest_root, all_modules, root_dir)

    if command == "git":
        git_extra_args = ' '.join(extra_args) if extra_args else ""

        if args.modules:
            modules_name_list = args.modules
        else:  # no modules list means all modules
            modules_name_list = git_module_names_list

        if args.mode not in git_modes_list:
            err_print("%s invalid, git command mode expected" % (args.mode))
        else:
            git_cmd_handler(modules_name_list, git_modules, args.mode,
                            script_dir, args.check, args.force, args.flow,
                            git_extra_args)

    elif command == "tree":
        if args.modules:
            modules_name_list = args.modules
            notfound = [
                m for m in modules_name_list
                if m not in cmake_module_names_list
            ]
            if notfound:
                err_print("modules:%s not found or can not be built" %
                          (str(notfound)))
        else:  # no modules list means all modules
            modules_name_list = cmake_module_names_list

        configure_exclude_list(build_info_exclude_defines_list, cmake_section)
        lang_list, lang_defs = configure_languages(cmake_section)
        if len(lang_list) == 0:
            lang_list["CXX", "C", "ASM"]
            lang_defs = {}

        if args.mode == "generate-cmake":
            tree_modules_list, single_modules_list = prepare_single_tree_lists(
                modules_name_list, cmake_modules, args.force)

            # args.cmake_vars come from make, allow all keys (force:False)
            cmake_defines_dict = {}
            if args.cmake_vars:
                add_user_defines(cmake_defines_dict, args.cmake_vars, True)

            if "cmake-defines" in cmake_section:
                add_user_defines(cmake_defines_dict,
                                 cmake_section["cmake-defines"])

            generate_tree_root_cmake(cmake_modules, tree_modules_list,
                                     cmake_defines_dict, project_section,
                                     cmake_section, lang_list, lang_defs,
                                     args.cmake_file_name)

        elif args.mode == "generate-make":
            if args.generator:
                cmake_generator = "Unix Makefiles" if args.generator == "make" else "Ninja"
            else:
                cmake_generator = "Unix Makefiles" if cmake_section[
                    "generator"] == "make" else "Ninja"

            cmake_defines_dict = {}
            cmake_defines_dict["-DCMAKE_INSTALL_PREFIX"] = project_section[
                "install-prefix"]

            # args.cmake_vars come from user, check forbidden keys (force:False)
            if args.cmake_vars:
                add_user_defines(cmake_defines_dict, args.cmake_vars)

            if "cmake-defines" in cmake_section:
                add_user_defines(cmake_defines_dict,
                                 cmake_section["cmake-defines"])

            cmake_build_opts = get_build_opts(extra_args)

            if args.verbose:
                add_user_defines(cmake_defines_dict, ["-DV=ON"])
                cmake_build_opts += " -v"

            tree_modules_list, single_modules_list = prepare_single_tree_lists(
                modules_name_list, cmake_modules, args.force)

            generate_tree_build_makefile(
                manifest_root, cmake_modules, tree_modules_list,
                single_modules_list, cmake_generator, cmake_defines_dict,
                cmake_build_opts, args.target if args.target else [],
                project_section["env"] if "env" in project_section else {},
                args.nodeps, args.noprebuild, args.nopostinstall, args.force)

        elif args.mode == "purge":
            generate_tree_purge_makefile(manifest_root, args.install_dir,
                                         args.build_dir)

        else:
            err_print("tree command expected\n")

    elif command == "module":
        # quiet_mode = False
        module_mode = args.mode

        configure_exclude_list(build_info_exclude_defines_list, cmake_section)
        lang_list, lang_defs = configure_languages(cmake_section)

        if module_mode == "build":
            nodeps = args.nodeps
            noprebuild = args.noprebuild
            nopostinstall = args.nopostinstall

            default_cmake_build_tgts_list = ["all"]
            if args.target:
                special_cmake_build_tgts_list = args.target
                if args.modules:
                    special_tgt_names_list = args.modules
                else:
                    special_tgt_names_list = cmake_module_names_list
            else:
                special_cmake_build_tgts_list = None
                special_tgt_names_list = []

            if args.generator:
                cmake_generator = "Unix Makefiles" if args.generator == "make" else "Ninja"
            else:
                cmake_generator = "Unix Makefiles" if cmake_section[
                    "generator"] == "make" else "Ninja"

            cmake_defines_dict = {}
            cmake_defines_dict["-DCMAKE_INSTALL_PREFIX"] = project_section[
                "install-prefix"]

            add_user_defines(cmake_defines_dict, lang_defs)

            if args.cmake_vars:
                add_user_defines(cmake_defines_dict, args.cmake_vars)

            if "cmake-defines" in cmake_section:
                add_user_defines(cmake_defines_dict,
                                 cmake_section["cmake-defines"])

            cmake_build_opts = get_build_opts(extra_args)

            if args.verbose:
                add_user_defines(cmake_defines_dict, ["-DV=ON"])
                cmake_build_opts += " -v"

            # if args.quiet:
            #     add_user_defines(cmake_defines_dict, ["-DCMAKE_INSTALL_MESSAGE=NEVER"])
            #     quiet_mode = True

            project_env = project_section[
                "env"] if "env" in project_section else {}

            if args.modules:
                req_modules_name_list = args.modules
                notfound = [
                    m for m in req_modules_name_list
                    if m not in cmake_module_names_list
                ]
                if notfound:
                    err_print("modules:%s not found or can not be built" %
                              (str(notfound)))
            else:  # no modules list means all modules
                req_modules_name_list = cmake_module_names_list

            modules_name_list, top_level_modules = create_build_subtree(
                req_modules_name_list, cmake_modules)

            force = args.force
            single_tree = False
            dev_mode = False

            generate_module_build_makefile(
                cmake_modules,
                modules_name_list,  # all modules to be built
                req_modules_name_list,  # originally requested modules
                top_level_modules,  # no other modules depend on these
                cmake_defines_dict,
                cmake_generator,
                cmake_build_opts,
                default_cmake_build_tgts_list,  # if no module 'build-targets' and no cmd line targets
                special_cmake_build_tgts_list,  # overrides 'build-targets' in module
                special_tgt_names_list,  # for which targets should we override
                project_env,
                nodeps,
                noprebuild,
                nopostinstall,
                force,
                single_tree,
                dev_mode)

        elif module_mode == "unpack":
            generate_module_unpack_makefile(project_section, all_modules,
                                            args.modules, args.nopostinstall,
                                            args.unpack_file)

        elif module_mode == "purge":
            if args.modules:
                modules_name_list = args.modules
            else:
                modules_name_list = cmake_module_names_list

            pack_options = ' '.join(extra_args) if extra_args else None

            generate_module_purge_makefile(cmake_modules, modules_name_list,
                                           pack_options, args.install_dir,
                                           args.build_dir, args.pack_files)
        else:
            err_print("module command expected\n")

    elif command == "list":
        list_mode = args.mode
        full_info = args.full
        if list_mode == "cmake":
            list_module_names(cmake_modules, full_info)
        elif list_mode == "git":
            list_module_names(git_modules, full_info)
        elif list_mode == "all":
            list_module_names(all_modules, full_info)
        else:
            err_usage("unsupported list subcommand {}\n".format(list_mode))

    elif command == "check":
        check_mode = args.mode
        check_modules_list = args.check_modules_list
        if len(sys.argv) > 5:  # at least one module name required
            if check_mode == "cmake":
                result, unexpected_module = list_modules_validation(
                    check_modules_list, cmake_modules)
            elif check_mode == "git":
                result, unexpected_module = list_modules_validation(
                    check_modules_list, git_modules)
            elif check_mode == "all":
                result, unexpected_module = list_modules_validation(
                    check_modules_list, all_modules)
            else:
                err_usage("Invalid command: project.py {}".format(check_mode))

            if not result:
                err_print("module %s not found\n" % (unexpected_module))

        else:
            err_usage("Invalid command: project.py %s" %
                      (' '.join(sys.argv[1:])))

    elif command == "get":
        node = manifest_root
        if args.mode == "property":
            for name in args.name_list:
                if name in node:
                    node = node[name]
                else:
                    err_print("BUG. unknown field: {}\n".format(
                        '.'.join.args.name_list))

            print(node)

        else:
            err_usage("unsupported get subcommand: {}\n".format(args.mode))
    else:
        err_usage("unexpected command: %s\n" % (command))

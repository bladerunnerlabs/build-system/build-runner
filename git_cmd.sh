#!/bin/bash
set -e -o pipefail

SCRIPT_PATH=$(readlink -e $0)
SCRIPT_DIR=$(dirname ${SCRIPT_PATH})

source ${SCRIPT_DIR}/utils.sh

function usage()
{
    ret_code=${1:-0}; shift

    echo -e "$@"
    echo -e "\nUsage: ./git_cmd [command] [options] -- <extra-opts>"

    echo -e "\nList of commands:"
    echo -e "\tfetch_sync - fetch modules for git sync (manifest or upstream)"
    echo -e "\tfetch_pull - fetch modules for git pull (upstream only)"
    echo -e "\tsync_prepare - prepare to sync modules (update using manifest or upstream)"
    echo -e "\tpull_prepare - prepare to pull modules (update using upstream only)"
    echo -e "\tpush - push modules to their git repositories"
    echo -e "\treset - git reset modules"
    echo -e "\tstatus - show module status"
    echo -e "\tdiff - show module diff"

    echo -e "\nList of elementary commands (generated by {sync|pull}_prepare):"
    echo -e "\checkout - checkout module"
    echo -e "\rebase - rebase module"
    echo -e "\upstream - set module upstream remote"

    echo -e "\nList of options:"
    echo -e "\t--root -D : project root Directory"
    echo -e "\t--dir -d : git local directory"
    echo -e "\t--url -u : git remote URL"
    echo -e "\t--remote -r : git remote repository name"
    echo -e "\t--revision -R : git revision name"
    echo -e "\t--branch -b : git full branch name incl. remote"
    echo -e "\t--option -o : extra options for git calls"
    echo -e "\t--check -c : just perform checks"
    echo -e "\t--force -f : proceed even after errors"

    echo -e "\n\t-- <extra-opts> are passed to git, e.g. --force, --name-only"
    echo -e "\n--help -h : list available commands and options\n"

    exit ${ret_code}
}

# options for cmd line args parser
short_op="u:D:d:r:R:b:o:cf"
long_op="url:,root:,dir:,remote:,revision:,branch:,option:,check,force"

# args supplied after --
extra_args=

# regular options
dir=
url=
remote=
revision=
branch=
option=
check=
force=
root=

function parse_arguments() {
    while [ $# -gt 0 ]; do
        case $1 in
                (-D | --root) root=$2; shift ;;
                (-d | --dir) dir=$2; shift ;;
                (-u | --url) url=$2; shift ;;
                (-r | --remote) remote=$2; shift ;;
                (-R | --revision) revision=$2; shift ;;
                (-b | --branch) branch="$2"; shift ;;
                (-o | --option) option="--$2"; shift ;;
                (-c | --check) check="yes"; ;;
                (-f | --force) force="yes"; ;;

                # default options
                (--) shift; break ;; # $@ contains remaining pos args
                (-*) usage 1 "error - unrecognized option $1" ;;
                (*) break ;;
        esac
        shift
    done
}

# git utility functions
# ---------------------

function git_cur_branch_name(){
    ${git_cmd} rev-parse --abbrev-ref HEAD
}

function git_head_is_branch(){
    [ "$(git_cur_branch_name)" != "HEAD" ]
}

function git_cur_branch_head_commit_id(){
    local length=$1

    local short_arg
    [ "${length}" == "short" ] && short_arg="--short"

    ${git_cmd} rev-parse --quiet ${short_arg} HEAD
}

function git_cur_head_describe(){
    ${git_cmd} describe --always HEAD
}

function git_remote_branch_head_commit_id(){
    local remote=$1; shift
    local revision=$1; shift
    local length=$1;

    local short_arg
    [ "${length}" == "short" ] && short_arg="--short"

    ${git_cmd} rev-parse --quiet ${short_arg} ${remote}/${revision}
}

function git_compare_local_commit_id_to_remote(){
    local remote=$1; shift
    local revision=$1;

    l_commit_id=$(git_cur_branch_head_commit_id)
    r_commit_id=$(git_remote_branch_head_commit_id ${remote} ${revision})

    [ "${l_commit_id}" == "${r_commit_id}" ]
}

function is_local_branch(){
    local rev=$1

    ${git_cmd} show-ref --verify --quiet "refs/heads/${rev}"
}

function is_remote_branch(){
    local remote=$1; shift
    local revision=$1;

    ${git_cmd} show-ref --verify --quiet "refs/remotes/${remote}/${revision}"
}

function git_uncommited_files(){
    [ -n "$(${git_cmd} update-index --refresh)" ]
}

function git_revision_type(){
    local revision=$1

    local rev_exist l_branch rev_tag

    ${git_cmd} rev-parse --verify --quiet ${revision} &>/dev/null && rev_exist=true
    if [ -n "${rev_exist}" ]; then
        # rev_id="$(${git_cmd} rev-parse ${revision})"
        # check if the 'revision' revision is a local branch
        ${git_cmd} show-ref --verify --quiet "refs/heads/${revision}" && l_branch=true
        if [ -n "${l_branch}" ]; then
            echo "local-branch"
        else
            # check if the 'revision' revision is a local tag
            ${git_cmd} show-ref --verify --quiet "refs/tags/${revision}" && rev_tag=true
            if [ -n "${rev_tag}" ]; then
                echo "tag"
            else
                # revision seems to be a commit id or an equivalent symbolic ref-spec
                echo "commit_id"
            fi
        fi
    else
        echo "error"
    fi
} # git_revision_type

function get_upstream_for_cur_head(){

    ${git_cmd} for-each-ref --format='%(upstream:short)' "$(${git_cmd} symbolic-ref -q HEAD)"
}

function get_upstream_for_branch(){
    local branch=$1

    ${git_cmd} for-each-ref --format='%(upstream:short)' refs/heads/${branch}
}

# git command handlers
# --------------------

function git_fetch() {
    local flow=$1
    local remote_url upstream_branch upstream_remote upstream_url

    if [ ! -d ${root}/${dir} ]; then
        echo_yellow "clone remote: ${remote} url: ${url}" | prefix_line
        git clone -c color.ui=always --quiet --no-single-branch ${extra_args} \
            --origin ${remote} ${url} ${root}/${dir} 2>&1 | prefix_line
        return $?
    fi

    
    # get upstream branch
    upstream_branch=$(get_upstream_for_cur_head)
    if [ -n "${upstream_branch}" ]; then
        upstream_remote="${upstream_branch%%/*}"
        # upstream_revision="${upstream_branch#*/}"
        upstream_url=$(${git_cmd} remote get-url ${upstream_remote} 2>/dev/null || echo)
    fi

    if [ "${flow}" == "sync" ]; then
        # get manifest remote's url
        remote_url=$(${git_cmd} remote get-url ${remote} 2>/dev/null || echo)

        if [[ -n "${remote_url}" && "${remote_url}" != "${url}" ]]; then
            # check remote settings in manifest
            if [ -z "${force}" ]; then
                echo_red "ERROR: current url: ${remote_url} of remote: ${remote} differs from manifest: ${url}" | prefix_line
                return 1
            else
                echo "current url: ${remote_url} of remote: ${remote} differs from manifest: ${url}" | prefix_line
                echo_yellow "remove remote: ${remote}" | prefix_line
                ${git_cmd} remote remove ${remote}
                remote_url=
            fi
        fi

        if [ -z "${remote_url}" ]; then
            echo_yellow "add remote: ${remote}, url: ${url}" | prefix_line
            ${git_cmd} remote add ${remote} ${url} 2>&1 | prefix_line
            remote_url=${url}
        fi

        # fetch upstream remote if different from manifest
        if [[ -n "${upstream_remote}" && "${upstream_remote}" != "${remote}" ]]; then
            echo "fetch upstream remote: ${upstream_remote} url: ${upstream_url}" | prefix_line
            ${git_cmd} fetch --tags ${extra_args} ${upstream_remote} 2>&1 | prefix_line
        fi
        # fetch remote from manifest
        echo "fetch remote: ${remote} url: ${url}" | prefix_line
        ${git_cmd} fetch --tags ${extra_args} ${remote} 2>&1 | prefix_line

    elif [ "${flow}" == "pull" ]; then
        if [ -n "${upstream_branch}" ]; then
            echo "fetch upstream remote: ${upstream_remote} url: ${upstream_url}" | prefix_line
            ${git_cmd} fetch --tags ${extra_args} ${upstream_remote} 2>&1 | prefix_line
        fi
    fi
} # git_fetch

function git_push(){

    if [ -n "$(${git_cmd} branch | grep ${revision})" ]; then
        ${git_cmd_color} checkout ${revision} 2>&1 | prefix_line
        ${git_cmd_color} push ${extra_args} ${remote} ${revision} 2>&1 | prefix_line
    else
        fatal_error 1 "${root}/${dir} has no branch ${revision}, can't push"
    fi
} # git_push

function git_diff(){

    ${git_cmd_color} diff ${extra_args} | prefix_line
} # git_diff

function git_reset(){

    ${git_cmd_color} reset ${extra_args} | prefix_line
} # git_reset

function git_status(){
    local cur_branch="$(${git_cmd_color} branch | grep '^* ' | sed 's/^* //')"
    local cur_branch_nocolor=$(echo "${cur_branch}" | sed -r "s/\x1B\[([0-9]{1,3}(;[0-9]{1,2})?)?[mGK]//g")

    local cur_branch_strip="${cur_branch_nocolor##(HEAD detached at }"
    cur_branch_strip="${cur_branch_strip%%)}"

    if [ "${cur_branch_strip}" == "${revision}" ]; then
        if [ "${cur_branch_strip}" == "${cur_branch_nocolor}" ]; then
            echo -e "revision: ${cur_branch}" | prefix_line
        else
            echo -e "revision: ${GREEN}${cur_branch_strip}${NC} (detached HEAD)" | prefix_line
        fi
    else
        echo -e "revision: '${cur_branch}' manifest: '${revision}'" | prefix_line
    fi

    head_log="$(${git_cmd_color} log --oneline -n1 HEAD)"
    if [[ ${#head_log} -lt 80 ]]; then
        echo -e "${head_log}" | prefix_line
    else
        echo -e "${head_log:0:80} ..." | prefix_line
    fi
    ${git_cmd_color} status --short --untracked-files=no ${extra_args} | prefix_line

    echo
} # git_status

function git_pull_prepare() {
    local detached_state cur_branch upstream_branch upstream_remote upstream_revision branch_eq all_committed

    git_head_is_branch || detached_state=true
    if [ -n "${detached_state}" ]; then
        echo_yellow "can't pull, detached state: $(git_cur_head_describe)" | prefix_line
        return 1
    fi

    cur_branch="$(git_cur_branch_name)"
    upstream_branch=$(get_upstream_for_cur_head)
    if [ -z "${upstream_branch}" ]; then
        echo_red "no upstream branch for: ${cur_branch}" | prefix_line
        echo_red "set upstream branch using 'git branch -u <upstream_branch>'" | prefix_line
        return 127
    fi
    upstream_remote="${upstream_branch%%/*}"
    upstream_revision="${upstream_branch#*/}"
    git_compare_local_commit_id_to_remote ${upstream_remote} ${upstream_revision} && branch_eq=true

    git_uncommited_files || all_committed=true

    if [ -z "${branch_eq}" ]; then # local and upstream diverged
        if [ -z "${all_committed}" ]; then
            echo_red "can't pull branch: ${upstream_branch}" | prefix_line
            echo_red "uncommitted changes exist; please commit or stash them" | prefix_line
            return 127
        fi
        if [ -n "${check}" ]; then
            echo_yellow "branch: ${cur_branch} may be updated by pull, from upstream: ${upstream_branch}" | prefix_line
            return 1
        fi

        echo "${SCRIPT_PATH} rebase --root ${root} -d ${dir} -b ${upstream_branch}"
        return 0

    else # local and remote synchonized
        echo_green "branch: ${cur_branch} is up to date, synchronized with upstream: ${upstream_branch}" | prefix_line
        if [ -z "${all_committed}" ]; then
            echo_yellow "uncommitted changes exist" | prefix_line
        fi
        return 1
    fi
} # git_pull_prepare

function git_sync_prepare() {
    local remote_br local_br cur_branch upstream_branch branch_eq all_committed upstream_incorrect

    # check manifest remote revision
    is_remote_branch ${remote} ${revision} && remote_br=true
    if [ -n "${remote_br}" ]; then
        git_uncommited_files || all_committed=true
        git_head_is_branch || detached_state=true
        is_local_branch ${revision} && local_br=true

        if [ -n "${local_br}" ]; then # local branch ${revision} exists
            # check if the current branch complies with manifest
            cur_branch="$(git_cur_branch_name)"
            if [ "${cur_branch}" != "${revision}" ]; then
                if [ -z "${force}" ]; then
                    if [ -z "${detached_state}" ]; then
                        echo_red "current branch: ${cur_branch} differs from manifest: ${revision}" | prefix_line
                    else
                        rev_id="$(${git_cmd} rev-parse HEAD^{})"
                        echo_red "currently detached at: ${rev_id} while manifest requires branch: ${revision}" | prefix_line
                    fi
                    return 127
                else
                    echo "${SCRIPT_PATH} checkout --root ${root} -d ${dir} --revision ${revision} && ${SCRIPT_PATH} rebase --root ${root} -d ${dir} -b ${remote}/${revision}"
                    return 0
                fi
            fi

            # check upstream branch - it should be set
            upstream_branch=$(get_upstream_for_cur_head)
            if [ -z "${upstream_branch}" ]; then
                if [ -z "${force}" ]; then
                    echo_red "no upstream branch for: ${cur_branch}" | prefix_line
                    echo_yellow "consider setting it to: ${remote}/${revision}" | prefix_line
                    echo_yellow "set upstream branch using 'git branch -u ${remote}/${revision}'" | prefix_line
                    echo_yellow "'project git sync --force' may fix this as well" | prefix_line
                    return 127
                fi
            fi

            # current branch name complies with manifest, but its upstream differs
            if [ "${upstream_branch}" != "${remote}/${revision}" ]; then
                upstream_incorrect=true
                if [ -z "${force}" ]; then
                    echo_red "upstream branch: ${upstream_branch} differs from manifest: ${remote}/${revision}" | prefix_line
                    echo_yellow "consider setting it to: ${remote}/${revision}" | prefix_line
                    echo_yellow "set upstream branch using 'git branch -u ${remote}/${revision}'" | prefix_line
                    echo_yellow "'project git sync --force' may fix this as well" | prefix_line
                    return 127
                fi
            fi

            git_compare_local_commit_id_to_remote ${remote} ${revision} && branch_eq=true
            if [[ -z "${branch_eq}" || -n "${upstream_incorrect}" ]]; then 
                # local and remote diverged or upstream is set incorrectly
                if [ -z "${all_committed}" ]; then
                    echo_red "can't rebase branch: ${revision}" | prefix_line
                    echo_red "uncommitted changes exist; please commit or stash them" | prefix_line
                    return 127
                fi

                if [ -n "${check}" ]; then
                    if [ -z "${branch_eq}" ]; then
                        echo_yellow "branch: ${revision} may be updated by rebase, from: ${remote}/${revision}" | prefix_line
                    else
                        echo_yellow "upstream for branch: ${revision} should be reset to: ${upstream_branch}" | prefix_line
                        echo_yellow "set upstream branch using 'git branch -u ${remote}/${revision}'" | prefix_line
                    fi
                    return 1
                else
                    echo "${SCRIPT_PATH} rebase --root ${root} -d ${dir} -b ${remote}/${revision}"
                    return 0
                fi
            else # local and remote synchronized
                echo_green "branch: ${revision} is up to date, synchronized with manifest: ${remote}/${revision}" | prefix_line
                if [ -z "${all_committed}" ]; then
                    echo_yellow "uncommitted changes exist" | prefix_line
                fi
                return 1
            fi
        else # local branch ${revision} not found, it should be checked out
            rev_id="$(${git_cmd} rev-parse HEAD^{})"
            
            if [ -z "${all_committed}" ]; then
                if [ -n "${detached_state}" ]; then
                    echo_red "current id: ${rev_id}" | prefix_line
                else
                    cur_branch="$(git_cur_branch_name)"
                    echo_red "current branch: ${cur_branch} (${rev_id})" | prefix_line
                fi
                echo_red "may be updated by checkout, from: ${remote}/${revision}" | prefix_line
                echo_red "uncommitted changes exist; please commit or stash them" | prefix_line
                return 127
            fi

            if [ -n "${check}" ]; then
                if [ -n "${detached_state}" ]; then
                    echo_yellow "current id: ${rev_id}" | prefix_line
                else
                    cur_branch="$(git_cur_branch_name)"
                    echo_yellow "current branch: ${cur_branch} (${rev_id})" | prefix_line
                fi
                echo_yellow "checkout and track new branch: ${remote}/${revision}" | prefix_line
                return 1
            else
                echo "${SCRIPT_PATH} checkout --root ${root} -d ${dir} -o track --revision '${remote}/${revision}'"
                return 0
            fi
        fi
    else # ${remote}/${revision} is not a remote branch, let's check ${revision} as a local refspec
        revision_t=$(git_revision_type ${revision})
        case ${revision_t} in
            tag | commit_id )
                git_head_is_branch || detached_state=true
                rev_id="$(${git_cmd} rev-parse ${revision}^{})"
                head_id="$(git_cur_branch_head_commit_id)"
                if [ -n "${check}" ]; then
                    if [ "${revision_t}" == "tag" ]; then
                        if [ "${head_id}" != "${rev_id}" ]; then
                            echo_yellow "may be updated by checkout, to tag: ${revision} (${rev_id})" | prefix_line
                        else
                            if [ -z "${detached_state}" ]; then
                                echo_yellow "may be updated by checkout, to tag: ${revision} (${rev_id})" | prefix_line
                            else
                                echo_green "up to date, at tag: ${revision} (${rev_id})" | prefix_line
                            fi
                        fi
                    else # commit-id
                        if [ "${head_id}" != "${rev_id}" ]; then
                            if [ "${rev_id}" == "${revision}" ]; then # plain commit-id
                                echo_yellow "may be updated by checkout, to revision: ${revision}" | prefix_line
                            else # refspec equivalent to commit-id
                                echo_yellow "may be updated by checkout, to revision: ${revision} (${rev_id})" | prefix_line
                            fi
                        else
                            if [ "${rev_id}" == "${revision}" ]; then # plain commit-id
                                echo_green "up to date, at: ${revision}" | prefix_line
                            else
                                if [ -z "${detached_state}" ]; then
                                    echo_yellow "may be updated by checkout, to revision: ${revision} (${rev_id})" | prefix_line
                                else
                                    echo_green "up to date, at revision: ${revision} (${rev_id})" | prefix_line
                                fi
                            fi
                        fi
                    fi
                    return 1
                fi

                # all the valid revision forms here are checked out as a detached head
                if [ "${head_id}" != "${rev_id}" ]; then
                    echo "${SCRIPT_PATH} checkout --root ${root} -d ${dir} -o detach --revision ${revision}"
                    return 0
                else # head and manifest correspond to the same commit-id
                    if [ "${rev_id}" == "${revision}" ]; then
                        # revision is a plain commit-id, nothing to do
                        echo_green "up to date, at revision: ${revision}" | prefix_line
                        return 1
                    elif [ -z "${detached_state}" ]; then
                        # current head is a branch while revision is a tag or other symbolic refspec
                        # check it out in detached mode to match the manifest
                        echo "${SCRIPT_PATH} checkout --root ${root} -d ${dir} -o detach --revision ${revision}"
                        return 0
                    else # current head is detached, so it virtually matches the manifest
                        echo_green "up to date, at revision: ${revision} (${rev_id})" | prefix_line
                        return 1
                    fi
                fi
                ;;

            local-branch ) # local branch without remote counterpart or mismatch
                # get upstream branch
                upstream_branch=$(get_upstream_for_branch ${revision})
                if [ -z "${upstream_branch}" ]; then
                    # no upstream branch
                    echo_red "no upstream branch for: ${revision}, which is local only" | prefix_line
                else
                    # there is an upstream branch but it differs from manifest
                    echo_red "upstream branch: ${upstream_branch} differs from manifest: ${remote}/${revision}" | prefix_line
                fi
                echo_yellow "consider setting it to: ${remote}/${revision}" | prefix_line
                echo_yellow "set upstream branch using 'git branch -u <upstream_branch>'" | prefix_line
                return 127
                ;;

            * ) # parsing failed, so opt out
                echo_red "error: revision ${remote} ${revision} not found" | prefix_line
                return 127
                ;;
        esac
    fi
} # git_sync_prepare

function git_checkout_cmd(){
    local cur_branch="$(git_cur_branch_name)"
    echo_yellow "branch: ${cur_branch}, checkout ${option} ${revision}" | prefix_line
    { ${git_cmd} checkout ${option} ${revision} 2>&1 || \
        { echo_red "checkout ${option} ${revision} failed"; exit 1; } \
    } | prefix_line
}

function git_rebase_cmd(){
    cur_branch="$(git_cur_branch_name)"
    cur_upstream="$(get_upstream_for_cur_head)"
    if [[ -z "${cur_upstream}" || "${cur_upstream}" != "${branch}" ]]; then
        git_set_upstream_cmd
    fi
    echo_yellow "branch: ${cur_branch}, rebase to ${branch}" | prefix_line
    ${git_cmd} rebase ${branch} 2>&1 | prefix_line
}

function git_set_upstream_cmd(){
    cur_branch="$(git_cur_branch_name)"
    echo_yellow "branch: ${cur_branch}, set upstream to ${branch}" | prefix_line
    ${git_cmd} branch -u ${branch} 2>&1 | prefix_line
}

if [ $# -lt 1 ]; then
    usage 1  "not enough arguments"
fi

# execute requested git command
# =============================

cmd=$1
shift

if [ "${cmd}" == "--help" ] || [ "${cmd}" == "-h" ]; then
    usage 0 ""
fi

# find custom parameters, passed after --
args="$@"
normal_args="${args%%-- *}" # all args until --
if [ "${normal_args}" != "${args}" ]; then
    extra_args="${args##*-- }" # all args after --
fi

if [ -n "${short_op}" ]; then
    options=$(getopt --options ${short_op} --long ${long_op} -- ${normal_args})
    if [ $? -ne 0 ]; then
        usage 1 "failed to parse: ${normal_args} for ${cmd}"
    fi
else
    options="${normal_args}"
fi

eval set -- ${options} # set new $@

parse_arguments "$@"

if [ -z "${root}" ]; then
    root=$(dirname ${SCRIPT_DIR}) # set default
fi

if [ ! -d ${root}/${dir} ]; then
    if [ ${cmd} != "fetch_sync" ] && [ ${cmd} != "fetch_pull" ]; then
        fatal_error 1 "ERROR. module directory ${root}/${dir} not found"
    fi
fi

git_cmd="git -C ${root}/${dir}"
git_cmd_color="${git_cmd} -c color.ui=always"

case $cmd in
    fetch_sync )
        git_fetch sync
        ;;
    fetch_pull )
        git_fetch pull
        ;;
    sync_prepare )
        git_sync_prepare
        ;;
    pull_prepare )
        git_pull_prepare
        ;;
    push )
        git_push
        ;;
    reset )
        git_reset
        ;;
    status )
        git_status
        ;;
    diff )
        git_diff
        ;;
    checkout )
        git_checkout_cmd
        ;;
    rebase )
        git_rebase_cmd
        ;;
    upstream )
        git_set_upstream_cmd
        ;;
    * )
        usage 1 "failed to parse: ${cmd}"
        ;;
esac
